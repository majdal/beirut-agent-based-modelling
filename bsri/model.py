from shapely.geometry import Point
import geopandas as gpd
import pandas as pd
import numpy as np

from mesa import Model
from mesa.space import MultiGrid
from mesa.datacollection import DataCollector

from agents import HousingUnitAgent, HouseholdAgent, InvestorAgent, RealtorAgent
from schedule import RandomActivationByBreed


def generate_price_grid(file, width, height):
    neighbourhoods_gdf = gpd.read_file(file)
    # extent = neighbourhoods_gdf.total_bounds
    east,  south, west,  north = neighbourhoods_gdf.total_bounds
    # total bounds fomratted [east,  south, west,  north]
    #                   e.g. [35.41, 33.80, 35.59, 33.96]
    density_x = (west - east)/width
    density_y = (north - south)/height
    offset_x = east
    offset_y = south
    price_grid = []

    # locations related to preference 
    aub = Point(35.4820, 33.9002)
    lau = Point(35.4783, 33.8928)
    usj = Point(35.5087, 33.8910)
    centre = Point(35.5054, 33.8975)

    for x in range(width):
        col = []
        for y in range(height):
            point = Point(offset_x + x*density_x + density_x*0.5,
                          offset_y + y*density_y + density_y*0.5)
            neighbourhood = None
            for row in neighbourhoods_gdf.iterrows():
                if row[1].geometry.intersects(point):
                    neighbourhood = row[1]
                    break
            # if the point falls within municipal Beirut boundaries
            try:
                unit_info = dict()
                unit_info['geometry'] = point
                unit_info['price 2017'] = neighbourhood['2017 price']
                unit_info['price 2018'] = neighbourhood['2018 price']
                unit_info['price 2019'] = neighbourhood['2019 price']
                unit_info['price'] = neighbourhood['2019 price']
                unit_info['neighbourhood_id'] = neighbourhood['neighbourhood_id']
                unit_info['neighbourhood_name'] = neighbourhood['name']
                # The following distances are euclidian distances, which is not very good
                # but for the sake of time, they're fine
                # Also they're using degrees instead of metric measurements, but since we're normalizing
                # that'll be fine as well. 
                # TODO potential improvement: use pandana to get the network distance, as per the following suggestions: https://github.com/UDST/pandana/issues/146
                unit_info['distance_aub'] = point.distance(aub)
                unit_info['distance_lau'] = point.distance(lau)
                unit_info['distance_usj'] = point.distance(usj)
                unit_info['distance_centre'] = point.distance(centre)
                col.append(unit_info)
            except TypeError:
                col.append(None)
        price_grid.append(col)
    prices = np.array(price_grid)
    np.save('price_grid', prices)
    return prices


class BSRIModel(Model):
    """ Buy/Sell/Rent/Invest Model

    A model that simulates the dynamics of a real estate market, 
    ultimately aiming to understand how it is affected by vacancy taxes. 

    It has five types of agents: 
    - Buyers: households seeking to purchase a housing unit to live in
    - Sellers: households that currently own an apartment and are looking to sell it
    - Renters: a landlord that owns a housing unit and is looking to rent it out
    - Tenants: a household is currently renting a housing unit
    - Investors: an agent that owns more than 1 housing unit

    This is a "Picasso" model (as opposed to a photograph model) [1], 
    meant to be complemented with GIS data as the next step of the research project. 

    [1] https://sci-hub.tw/https://doi.org/10.1016/j.envsoft.2015.10.005

    """

    def __init__(self, width=100, height=100, 
                       seed=None,
                       verbose=False,
                       save_data=False,
                       export_geojson=False,
                       months_in_timestep=12,
                       # city parameters
                       central_bank_interest_rate=0.08,
                       minimum_rent_contract_length=3,
                       vacancy_rate=0.23, 
                       tax_rate_vacancy=0.02,
                       tax_rate_property=0.01,
                       tax_rate_property_vacant=0.0,
                       tax_rate_transfer=0.01,
                       renter_buyer_ratio=1/3,
                       new_households_per_1k_units_per_step=10,
                       households_give_up_search_after=4,
                       allow_evictions=True,
                       force_average_prices_down=False,
                       wealth_distribution_shape=1.11,
                       operating_costs=0.0005,
                       operating_costs_vacant=0.8,
                       rent_price_from_sale_price=0.004,
                       # households
                       interest_rate_households=0.05,
                       default_mortgage_period=30,
                       minimum_household_budget=600,
                       household_pref_amenities=0.5,
                       household_units_to_sample=10,
                       # corporate investors
                       investor_corporate_interest_rate=0.09,
                       investor_corporate_mortgage_share=0.7,
                       investor_corporate_discount_factor=0.9,
                       investor_corporate_loss_aversion=1,
                       investor_corporate_probability_weight=0.8,
                       # amateur investors
                       investor_amateur_interest_rate=0.11,
                       investor_amateur_mortgage_share=0.1,
                       investor_amateur_discount_factor=0.8,
                       investor_amateur_loss_aversion=2.25,
                       investor_amateur_probability_weight=0.7,
                       file='./neighbourhoods_clean.geojson'):
        # , extent=[[35.46,33.91], [35.54, 33.86]]
        """ Create a new model. 
        Number of owners is equal to the number of cells. Buyers are ex

        ~23% of housing units are owned by investors, the rest are owned by individuals 

        Args: 
            width, height: dimensions of the grid
            months_in_timestep: temporal resolution of the model. 
            vacancy_rate: initial vacancy rate in the model
            interest_rate_households: average interest rate for households 
            file: file with the prices per neighbourhood
            minimumm_household_budget: the minimmum budget per month for either rent or purchase 
            wealth_distribution_shape: alpha, the shape of the Pareto distribution used. 
                                       Default is 1.11, which produces a the official Gini coefficient of Lebanon (0.819)
            initial_unhoused_ratio: how many households, as a percent of number of households 
        """
        super().__init__()
        if verbose:
            print('>>>>> initializing model...')
            print('________________________________________________')
        
        self.np_random = np.random.default_rng(seed)
        self.stop_running_after_steps = 20
        self.save_data = save_data
        self.export_geojson = export_geojson
        self.width = width
        self.height = height
        self.verbose = verbose
        self.months_in_timestep = months_in_timestep
        # From the Beirut Urban Lab data, I infer an average apartment size of 70m^2, with 15m^2 standard deviation
        self.average_unit_area = 70
        self.grid = MultiGrid(width, height, False)
        self.vacancy_rate = vacancy_rate
        self.schedule = RandomActivationByBreed(self)
        self.market_rent = list()
        self.market_sale = list()
        self.market_rent_prices = list()
        self.market_sale_prices = list()
        self.arrivals = 0
        self.departures = list()
        self.step_departures = list()
        self.step_flipped_units = list()
        self.transaction_count_rent = 0
        self.transaction_count_sale = 0
        self.transaction_count_rent_cumulative = 0
        self.transaction_count_sale_cumulative = 0

        # TODO include a central fund where taxes are deposited

        # CITY PARAMETERS
        self.central_bank_interest_rate = central_bank_interest_rate
        self.renter_buyer_ratio = renter_buyer_ratio
        self.renter_buyer_ratio_initial = 1/3
        self.tax_rate_vacancy = tax_rate_vacancy
        self.tax_rate_property = tax_rate_property
        self.tax_rate_property_vacant = tax_rate_property_vacant
        self.tax_rate_transfer = tax_rate_transfer
        self.wealth_distribution_shape = wealth_distribution_shape
        self.new_households_per_1k_units_per_step = new_households_per_1k_units_per_step
        self.households_give_up_search_after = households_give_up_search_after # time steps
        self.allow_evictions = allow_evictions
        self.only_switch_in_good_trend = False
        self.force_average_prices_down = force_average_prices_down
        self.max_bid_count_limit = 30
        self.operating_costs = operating_costs
        self.operating_costs_vacant = operating_costs_vacant # % of operating costs while occupied
        self.rent_price_from_sale_price = rent_price_from_sale_price

        self.max_dist_aub = 0
        self.max_dist_lau = 0
        self.max_dist_usj = 0
        self.max_dist_centre = 0

        self.max_proximity_aub = 0
        self.max_proximity_lau = 0
        self.max_proximity_usj = 0
        self.max_proximity_centre = 0

        # GENERAL INVESTOR PARAMETERS 
        # probability a household getting evicted/choosing to move after 3 time steps (minimum legal length of contract)
        self.minimum_rent_contract_length = minimum_rent_contract_length
        self.probability_eviction = 0.1
        self.threshold_amateur_corporate_investors = 4

        # HOUSEHOLD PARAMETERS
        self.minimum_household_budget = minimum_household_budget
        self.interest_rate_households = interest_rate_households
        self.household_pref_amenities = household_pref_amenities
        self.household_units_to_sample = household_units_to_sample
        self.default_mortgage_period = default_mortgage_period
        self.steps_in_search_before_leaving = 4

        # CORPORATE INVESTOR PARAMETERS
        self.investor_corporate_interest_rate = investor_corporate_interest_rate
        self.investor_corporate_mortgage_share = investor_corporate_mortgage_share
        self.investor_corporate_discount_factor = investor_corporate_discount_factor
        self.investor_corporate_loss_aversion = investor_corporate_loss_aversion
        self.investor_corporate_probability_weight = investor_corporate_probability_weight

        # AMATEUR INVESTOR PARAMETERS
        self.investor_amateur_interest_rate = investor_amateur_interest_rate
        self.investor_amateur_mortgage_share = investor_amateur_mortgage_share
        self.investor_amateur_discount_factor = investor_amateur_discount_factor
        self.investor_amateur_loss_aversion = investor_amateur_loss_aversion
        self.investor_amateur_probability_weight = investor_amateur_probability_weight

        try:
            price_grid = np.load('price_grid.npy', allow_pickle=True,)
            if self.verbose:
                print('loaded price grid file!')
        except FileNotFoundError:
            if self.verbose:
                print('generating a fresh price grid file')
            price_grid = generate_price_grid(file, self.width, self.height)

        # create realtor agents
        neighbourhoods = gpd.read_file(file)
        neighbourhoods['average unit area'] = pd.cut(neighbourhoods['2019 price'], bins=3, labels=[90, 140, 220])
        for row in neighbourhoods.iterrows():
            neighbourhood = row[1]
            price_history_sale = [neighbourhood['2015 price'], 
                                  neighbourhood['2016 price'], 
                                  neighbourhood['2017 price'], 
                                  neighbourhood['2018 price'], ]
                                #   neighbourhood['2019 price']]
            price_history_rent = map(lambda n: n * self.rent_price_from_sale_price, price_history_sale)
            realtor = RealtorAgent(neighbourhood['neighbourhood_id'],
                                   self,
                                   neighbourhood['name'],
                                   price_history_sale,
                                   list(price_history_rent),
                                   geometry=neighbourhood.geometry,
                                   average_unit_area=neighbourhood['average unit area'])
            self.schedule.add(realtor)
            self.current_id = max([self.current_id, neighbourhood['neighbourhood_id']])

        # Assign a housing unit to each grid cell within the boundaries of Beirut
        if self.verbose:
            print('>>>>> Create the housing units')
        for cell in self.grid.coord_iter():
            pos = (cell[1], cell[2])
            if price_grid[pos]:
                neighbourhood_id = price_grid[pos]['neighbourhood_id']
                neighbourhood_name = price_grid[pos]['neighbourhood_name']
                housing_unit = HousingUnitAgent(self.next_id(), self, 
                                                neighbourhood_name,
                                                neighbourhood_id,
                                                geometry=price_grid[pos]['geometry'],
                                                for_rent=False, 
                                                for_sale=False,
                                                distance_aub=price_grid[pos]['distance_aub'],
                                                distance_lau=price_grid[pos]['distance_lau'],
                                                distance_usj=price_grid[pos]['distance_usj'],
                                                distance_centre=price_grid[pos]['distance_centre'],)

                self.grid.place_agent(housing_unit, pos)
                self.schedule.add(housing_unit)

                housing_unit.realtor.all_housing_units.append(housing_unit)
                # set max distances 
                if price_grid[pos]['distance_aub'] > self.max_dist_aub:
                    self.max_dist_aub = price_grid[pos]['distance_aub']
                if price_grid[pos]['distance_lau'] > self.max_dist_lau:
                    self.max_dist_lau = price_grid[pos]['distance_aub']
                if price_grid[pos]['distance_usj'] > self.max_dist_usj:
                    self.max_dist_usj = price_grid[pos]['distance_usj']
                if price_grid[pos]['distance_centre'] > self.max_dist_centre:
                    self.max_dist_centre = price_grid[pos]['distance_centre']

        # set model metadata: max proximities and unit counts
        # proximity = distance_max + 1 - distance
        self.max_proximity_aub = self.max_dist_aub + 1
        self.max_proximity_lau = self.max_dist_lau + 1
        self.max_proximity_usj = self.max_dist_usj + 1
        self.max_proximity_centre = self.max_dist_centre + 1
        self.n_housing_units = len(self.schedule.get_breed_agents(HousingUnitAgent))

        if self.verbose:
            print('>>>>> Assigning owned housing units')
        all_units = self.schedule.get_breed_agents(HousingUnitAgent)
        n_owned_units = int(len(all_units)*(1-self.renter_buyer_ratio_initial))
        owned_units = self.random.sample(all_units, n_owned_units)
        steps_occupied = self.np_random.poisson(4, n_owned_units)
        for unit, n_steps in zip(owned_units, steps_occupied):
            owner = HouseholdAgent(self.next_id(), self, 'owner', housed_at=unit)
            self.schedule.add(owner)
            unit.owner = owner
            unit.tenant = owner
            unit.steps_occupied = n_steps
            unit.steps_occupied_initial = n_steps

        if self.verbose:
            print('>>>>> Assigning the rest of units as investments')
        count = 0
        n_non_owned_units = self.n_housing_units - len(self.vacancies)

        # give units to investors
        while count <= n_non_owned_units:
            n_units_owned = int(self.random.paretovariate(2.5))
            units_not_owned = [unit for unit in self.schedule.get_breed_agents(HousingUnitAgent) if not unit.owner]
            housing_units_owned = self.random.sample(units_not_owned, min(n_units_owned, len(units_not_owned)))
            investor_category = 'corporate' if n_units_owned > self.threshold_amateur_corporate_investors else 'amateur'
            investor = InvestorAgent(self.next_id(), self, investor_category)
            investor.housing_units_owned = housing_units_owned
            for unit in housing_units_owned:
                unit.owner = investor
            self.schedule.add(investor)
            count += n_units_owned

        # assign vacant units
        n_vacant_housing_units = int(len(self.schedule.get_breed_agents(HousingUnitAgent))*self.vacancy_rate)
        markets = self.random.choices(['rent', 'sale', 'wait'], 
                                        weights=[2/23, 13/23, 8/23], # average values from Beirut Urban Lab research 
                                        k=n_vacant_housing_units)
        vacant_units = self.random.sample(self.vacancies, n_vacant_housing_units)
        steps_in_market = self.np_random.poisson(0.5, n_vacant_housing_units)
        for unit, market, n_steps in zip(vacant_units, markets, steps_in_market):
            unit.steps_in_market = n_steps
            # assign some units to be for rent, others for sale
            if market == 'rent':
                unit.for_rent = True
                unit.for_sale = False
            elif market == 'sale':
                unit.for_sale = True
                unit.for_rent = False
            else:
                unit.for_sale = False
                unit.for_rent = False
                unit.keep_empty = True

        for unit in all_units:
            if type(unit.owner) is InvestorAgent and not (unit.for_rent or unit.for_sale) and not unit.keep_empty:
                unit.for_rent = False
                unit.for_sale = False
                tenant = HouseholdAgent(self.next_id(), self, 'tenant', budget=unit.price_rental, housed_at=unit)
                self.schedule.add(tenant)
                unit.tenant = tenant

        # populate sale and rental history for data integrity 
        for realtor in self.schedule.get_breed_agents(RealtorAgent):
            sampled_units = self.random.sample(realtor.all_housing_units, len(realtor.average_house_price_sale))
            for i, unit in enumerate(sampled_units):
                steps_in_market_sale = self.np_random.poisson(0.7, 1)[0]
                steps_in_market_rent = self.np_random.poisson(0.5, 1)[0]
                unit.price = realtor.average_house_price_sale[i] * unit.area
                unit.price_rental = realtor.average_house_price_rent[i] * unit.area
                record_sale = {
                    'unit_id': unit.unique_id,
                    'area': unit.area,
                    'price': unit.price,
                    'steps_in_market': steps_in_market_sale,
                }
                record_rent = {
                    'unit_id': unit.unique_id,
                    'area': unit.area,
                    'price': unit.price_rental,
                    'steps_in_market': steps_in_market_rent,
                }
                realtor.housing_units_sold.append([record_sale, ])
                realtor.housing_units_rented.append([record_rent, ])



        # Create a data collector that records
        self.datacollector = DataCollector(
            model_reporters={
                "unhoused tenants": lambda model: len(model.households_without_homes),
                "vacancies": lambda model: len(model.vacancies),
                "rental transactions": lambda model: model.transaction_count_rent_cumulative,
                "sale transactions": lambda model: model.transaction_count_sale_cumulative,
                "rentals": lambda model: round((model.transaction_count_rent/model.n_housing_units)*100, 2),
                "sales": lambda model: round((model.transaction_count_sale/model.n_housing_units)*100, 2),
                "vacancy total": lambda model: round((len(self.vacancies)/model.n_housing_units)*100, 2), 
                "vacancy (off market)": lambda model: round((len([unit for unit in self.vacancies if unit.on_market == 'no' and isinstance(unit.owner, InvestorAgent)])/model.n_housing_units)*100, 2), 
                "vacancy (for sale)": lambda model: round((len([unit for unit in self.vacancies if unit.on_market == 'sale'])/model.n_housing_units)*100, 2), 
                "vacancy (for rent)": lambda model: round((len([unit for unit in self.vacancies if unit.on_market == 'rent'])/model.n_housing_units)*100, 2), 
                "departures": lambda model: int(model.datacollector.get_table_dataframe('departures')['id'].count()),
                "step departures": lambda model: len(model.step_departures),
            },
            tables={
                "prospects": ['investor id', 'step', 'gamma', 'lambda', 'discount factor', 'optimism factor', 
                              'interest rate', 'mortgage share', 'operating costs', 
                              'tax vacancy', 'tax property', 'tax property vacant', 'tax transfer',
                              'asking price rent', 'asking price sale', 'price', 'rent', 'area', 
                              'neighbourhood', 'neigbhourhood average sale', 'neigbhourhood trend sale', 
                              'neigbhourhood average history rent', 'neigbhourhood average history sale',
                              'ror sale', 'ror rent', 'ror wait', 
                              'prob sale', 'prob rent',
                              'ptv sale', 'ptv rent', 'ptv wait', 'ptv alt', 'choice', 'ptv choice'], 
                "housing unit ownership": 
                    ['step', 'unit id', 'geometry', 'area', 'pos', 'x', 'y', 'legend', 'tenure type', 
                     'investor category', 'investor id', 'on market', 'price rent', 'price sale', 
                     'price sale init', 'price rent init', 'rent delta', 'sale delta'],
                "arrivals": ['id', 'budget', 'arrived at step', 'units to sample'],
                "departures": ['id', 'budget', 'departed at step', 'units to sample'],
                "residents": ['id', 'step', 'budget', 'home', 'tenure'],
                "transactions": ['unit_id', 'transaction type', 'area', 'price', 'steps_in_market', 'step'],
                "neighbourhoods": ['id', 'neighbourhood', 'geometry', 'average unit area', 'average price', 
                                   'average rent', 'average price history', 'average rent history', 
                                   'vacancy rate', 'vacancy rate normalized', 'unit count'],
            })
            # agent_reporters={"tenure type": "tenure_type"})

        if self.verbose:
            print('>>>>> start with some households already searching ')
        # do this to kick off the market 
        average_new_arrivals = self.new_households_per_1k_units_per_step * (self.schedule.get_breed_count(HousingUnitAgent)/1000)
        for breed in self.random.choices(['tenant', 'buyer'], 
                                         weights=[self.renter_buyer_ratio, 1-self.renter_buyer_ratio], 
                                         k=self.np_random.poisson(average_new_arrivals)):
            household = HouseholdAgent(self.next_id(), self, breed)
            household.steps_in_search = self.random.randint(1,4)
            self.schedule.add(household)
            self.arrivals += 1
            household.record_arrival()

        # initialize the collection process
        self.datacollector.collect(self)
        # capture the distribution at the beginning 
        # self.schedule.step_breed(HousingUnitAgent, step_name='record_data')

        
        # add initial prices just for the visualization 
        self.market_rent = self.units_for_rent
        self.market_sale = self.units_for_sale
        self.market_rent_prices = [unit.asking_price_rent for unit in self.market_rent]
        self.market_sale_prices = [unit.asking_price_sale for unit in self.market_sale]

        print(f'>>>>> initialized with {len(self.schedule.get_breed_agents(HousingUnitAgent))} housing units')

    @property
    def realtors(self):
        return self.schedule.agents_by_breed[RealtorAgent]

    @property
    def households_without_homes(self):
        return [household for household in self.schedule.get_breed_agents(HouseholdAgent) if not household.is_housed]

    @property
    def seeking_buyers(self):
        return [household for household in self.households_without_homes if household.breed == 'buyer']

    @property
    def seeking_tenants(self):
        return [household for household in self.households_without_homes if household.breed == 'tenant']

    @property
    def units_for_rent(self):
        return [housing_unit for housing_unit in self.schedule.get_breed_agents(HousingUnitAgent) if housing_unit.for_rent]

    @property
    def units_for_sale(self):
        return [housing_unit for housing_unit in self.schedule.get_breed_agents(HousingUnitAgent) if housing_unit.for_sale]

    @property
    def vacancies(self):
        return [housing_unit for housing_unit in self.schedule.get_breed_agents(HousingUnitAgent) if housing_unit.is_vacant]
    
    @property
    def investments(self):
        return [housing_unit for housing_unit in self.schedule.get_breed_agents(HousingUnitAgent) if housing_unit.tenure_type == 'investment' and not housing_unit.tenant]

    @property
    def current_inhabitants(self):
        return [household for household in self.schedule.get_breed_agents(HouseholdAgent) if household.is_housed]

    def owned_units(self):
        return [housing_unit for housing_unit in self.schedule.get_breed_agents(HousingUnitAgent) if housing_unit.tenure_type == 'ownership']

    def add_investor(self, category):
        investor = InvestorAgent(self.next_id(), self, category)
        self.schedule.add(investor)
        return investor

    def sample_units_rent(self, target_price, number_of_units):
        available_units = [{'id': unit.unique_id, 
                            'price': unit.asking_price_rent, 
                            'unit': unit} for unit in self.market_rent if len(unit.bids_rent) <= self.max_bid_count_limit]
        if len(available_units) > 0:
            units_df_rent = pd.DataFrame.from_records(available_units)
            sampled_units = units_df_rent.iloc[(units_df_rent['price']-target_price).abs().argsort()[:number_of_units]]
        else:
            # return an empty dataframe :'(
            sampled_units = pd.DataFrame(columns=['unit'])
        return sampled_units['unit']

    def sample_units_sale(self, target_price, number_of_units):
        available_units = [{'id': unit.unique_id, 
                            'price': unit.asking_price_sale, 
                            'unit': unit} for unit in self.market_sale if len(unit.bids_sale) <= self.max_bid_count_limit]
        if len(available_units) > 0:
            units_df_rent = pd.DataFrame.from_records(available_units)
            sampled_units = units_df_rent.iloc[(units_df_rent['price']-target_price).abs().argsort()[:number_of_units]]
        else:
            # return an empty dataframe :'(
            sampled_units = pd.DataFrame(columns=['unit'])
        return sampled_units['unit']

    def step(self):
        """ Advance the model by a single step. 
        """
        # since we're using schedule.step_breed() instead of .step(), we need to increment time manually
        self.schedule.steps += 1
        self.schedule.time += 1
        # if self.schedule.steps >= 10: 
        #     self.new_households_per_1k_units_per_step = 20
        # 1) Realtor determine the going sale/rent prices for their neighbourhood
        if self.verbose:
            print('>>>>> Realtors calculate the trends for their neighbourhood')
        self.schedule.step_breed(RealtorAgent)

        # 2) New households arrive into our city seeking housing
        # TODO how many new agents arrive per time step? I put 20 here, but why? 
        if self.verbose:
            print('>>>>> New households arrive in the city')
        average_new_arrivals = self.new_households_per_1k_units_per_step * (self.schedule.get_breed_count(HousingUnitAgent)/1000)
        for breed in self.random.choices(['tenant', 'buyer'], 
                                         weights=[self.renter_buyer_ratio, 1-self.renter_buyer_ratio], 
                                         k=self.np_random.poisson(average_new_arrivals)):
            household = HouseholdAgent(self.next_id(), self, breed)
            self.schedule.add(household)
            self.arrivals += 1
            household.record_arrival()

        # 3) Place units in the market
        if self.verbose:
            print('>>>>> Investors step')
        self.schedule.step_breed(InvestorAgent)

        self.market_rent = self.units_for_rent
        self.market_sale = self.units_for_sale

        # 4) Households evaluate the offers and make bids 
        if self.verbose:
            print('>>>>> Households step')
        self.schedule.step_breed(HouseholdAgent)

        # 5) Owners evaluate the bids, make deals, and register the deals 
        if self.verbose:
            print('>>>>> Housing units step')
        self.schedule.step_breed(HousingUnitAgent)

        if self.verbose:
            print('>>>>> Realtors save records from this time step')
        self.schedule.step_breed(RealtorAgent, step_name='step_save_records')
        
        # Finally collect the a snapshot of the relevant data
        self.datacollector.collect(self)

        # clear the markets for the next iteration
        self.market_rent_prices = [unit.asking_price_rent for unit in self.investments]
        self.market_sale_prices = [unit.asking_price_sale for unit in self.investments]
        self.market_sale.clear()
        self.market_rent.clear()
        self.step_departures.clear()
        self.step_flipped_units.clear() 
        if self.verbose:
            print(f'ℹ️ recorded {self.transaction_count_rent} rent transactions')
            print(f'ℹ️ recorded {self.transaction_count_sale} sale transactions')
        self.transaction_count_rent = 0
        self.transaction_count_sale = 0
        if self.verbose:
            print('>>>>> -------------- step complete')
        # stop the simulation after n steps 
        if self.schedule.steps > self.stop_running_after_steps - 1:
            # record some data
            self.schedule.step_breed(HousingUnitAgent, step_name='record_data')
            self.schedule.step_breed(HouseholdAgent, step_name='record_residency')
            self.schedule.step_breed(RealtorAgent, step_name='record_state')
            self.running = False
            print(f'>>>>> -------------- SIMULATION COMPLETE AFTER {self.stop_running_after_steps} STEPS')
            if self.save_data:
                # saving the data
                print(f'>>>>> -------------- Saving the data...')
                import os
                from datetime import datetime
                
                timestamp = datetime.now().strftime("%Y%m%d%H%M")
                # dir_name = f'data-{timestamp}'
                dir_path = os.path.join('data', f'data-{timestamp}')
                os.mkdir(dir_path)
                file_location = lambda file: os.path.join(dir_path, file)

                parameters = pd.DataFrame(columns=[
                       'seed', 'months_in_timestep', 'central_bank_interest_rate', 'minimum_rent_contract_length', 
                       'tax_rate_vacancy', 'tax_rate_property', 'tax_rate_property_vacant', 'tax_rate_transfer', 'renter_buyer_ratio',
                       'new_households_per_1k_units_per_step', 'households_give_up_search_after', 'force_average_prices_down', 'wealth_distribution_shape',
                       # households
                       'interest_rate_households', 'default_mortgage_period', 'minimum_household_budget', 'household_pref_amenities', 'household_units_to_sample',
                       # corporate investors
                       'investor_corporate_interest_rate', 'investor_corporate_mortgage_share', 'investor_corporate_discount_factor', 'investor_corporate_loss_aversion', 'investor_corporate_probability_weight',
                       # amateur investors
                       'investor_amateur_interest_rate', 'investor_amateur_mortgage_share', 'investor_amateur_discount_factor', 'investor_amateur_loss_aversion', 'investor_amateur_probability_weight'
                ])
                parameters.append({
                       'seed': self._seed,
                       'months_in_timestep': self.months_in_timestep,
                       # city parameters
                       'central_bank_interest_rate': self.central_bank_interest_rate,
                       'minimum_rent_contract_length': self.minimum_rent_contract_length,
                       'tax_rate_vacancy': self.tax_rate_vacancy,
                       'tax_rate_property': self.tax_rate_property,
                       'tax_rate_property_vacant': self.tax_rate_property_vacant,
                       'tax_rate_transfer': self.tax_rate_transfer,
                       'renter_buyer_ratio': self.renter_buyer_ratio,
                       'new_households_per_1k_units_per_step': self.new_households_per_1k_units_per_step,
                       'households_give_up_search_after': self.households_give_up_search_after,
                       'force_average_prices_down': self.force_average_prices_down,
                       'wealth_distribution_shape': self.wealth_distribution_shape,
                       # households
                       'interest_rate_households': self.interest_rate_households,
                       'default_mortgage_period': self.default_mortgage_period,
                       'minimum_household_budget': self.minimum_household_budget,
                       'household_pref_amenities': self.household_pref_amenities,
                       'household_units_to_sample': self.household_units_to_sample,
                       # corporate investors
                       'investor_corporate_interest_rate': self.investor_corporate_interest_rate,
                       'investor_corporate_mortgage_share': self.investor_corporate_mortgage_share,
                       'investor_corporate_discount_factor': self.investor_corporate_discount_factor,
                       'investor_corporate_loss_aversion': self.investor_corporate_loss_aversion,
                       'investor_corporate_probability_weight': self.investor_corporate_probability_weight,
                       # amateur investors
                       'investor_amateur_interest_rate': self.investor_amateur_interest_rate,
                       'investor_amateur_mortgage_share': self.investor_amateur_mortgage_share,
                       'investor_amateur_discount_factor': self.investor_amateur_discount_factor,
                       'investor_amateur_loss_aversion': self.investor_amateur_loss_aversion,
                       'investor_amateur_probability_weight': self.investor_amateur_probability_weight,
                }, ignore_index=True)
                parameters.to_pickle(os.path.join(dir_path, f'model_parameters_{timestamp}.p'))    

                model_data = self.datacollector.get_model_vars_dataframe()
                model_data.to_pickle(os.path.join(dir_path, f'model_vars_{timestamp}.p'))

                ownership_data = self.datacollector.get_table_dataframe('housing unit ownership')
                ownership_data.to_pickle(os.path.join(dir_path, f'ownership_{timestamp}.p'))

                arrivals_data = self.datacollector.get_table_dataframe('arrivals')
                arrivals_data.to_pickle(os.path.join(dir_path, f'arrivals_{timestamp}.p'))

                departures_data = self.datacollector.get_table_dataframe('departures')
                departures_data.to_pickle(os.path.join(dir_path, f'departures_{timestamp}.p'))

                residents_data = self.datacollector.get_table_dataframe('residents')
                residents_data.to_pickle(os.path.join(dir_path, f'residents_{timestamp}.p'))

                rentals_data = self.datacollector.get_table_dataframe('rentals')
                rentals_data.to_pickle(os.path.join(dir_path, f'rentals_{timestamp}.p'))

                sales_data = self.datacollector.get_table_dataframe('sales')
                sales_data.to_pickle(os.path.join(dir_path, f'sales_{timestamp}.p'))

                neighbourhoods_data = self.datacollector.get_table_dataframe('neighbourhoods')
                neighbourhoods_data.to_pickle(os.path.join(dir_path, f'neighbourhoods_{timestamp}.p'))

            if self.export_geojson:
                from datetime import datetime
                import os
                timestamp = datetime.now().strftime("%Y%m%d%H%M")
                dir_path = os.path.join('data', f'data-{timestamp}')
                os.mkdir(dir_path)

                neighbourhoods_data = self.datacollector.get_table_dataframe('neighbourhoods')
                neighbourhoods_data_geo = gpd.GeoDataFrame(neighbourhoods_data)
                neighbourhoods_data_geo = neighbourhoods_data_geo.set_crs(epsg=4326)
                del neighbourhoods_data_geo['average price history']
                del neighbourhoods_data_geo['average rent history']
                neighbourhoods_data_geo.to_file(os.path.join(dir_path, f'neighbourhoods_{timestamp}.geojson'), driver='GeoJSON')

                ownership_data = self.datacollector.get_table_dataframe('housing unit ownership')
                ownership_data_geo = gpd.GeoDataFrame(ownership_data)
                ownership_data_geo = ownership_data_geo.set_crs(epsg=4326)
                del ownership_data_geo['pos']
                ownership_data_geo.to_file(os.path.join(dir_path, f'ownership_{timestamp}.geojson'), driver='GeoJSON')