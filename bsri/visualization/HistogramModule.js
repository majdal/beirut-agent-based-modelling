// copied from https://github.com/projectmesa/mesa/blob/2503eceb6fb8a07d43d6a1d32b56a27474425475/docs/tutorials/adv_tutorial.ipynb

var HistogramModule = function (label, bins, canvas_width, canvas_height) {
  // Create the tag:
  var canvas_tag = `<canvas 
                      width="${canvas_width}" 
                      height="${canvas_height}"
                      style="border:1px dotted">
                    </canvas>`
  // Append it to #elements:
  var canvas = $(canvas_tag)[0]
  $("#elements").append(canvas)
  // Create the context and the drawing controller:
  var context = canvas.getContext("2d")

  // Prep the chart properties and series:
  var datasets = [
    {
      label: label,
      fillColor: "hsla(246, 66%, 58%, 1)",
      strokeColor: "hsla(246, 66%, 58%, 1)",
      highlightFill: "rgba(151,187,205,0.75)",
      highlightStroke: "rgba(151,187,205,1)",
      data: [],
    },
  ]

  // Add a zero value for each bin
  for (var i = 0; i < bins; i++) datasets[0].data.push(0)
  
  // Create the labels
  var labels = []

  var data = {
    labels: labels,
    datasets: datasets,
  }

  var options = {
    scaleBeginsAtZero: true,
  }

  // Create the chart object
  var chart = new Chart(context, { type: "bar", data: data, options: options })

  this.render = function (new_data) {
    data['labels'] = new_data['labels']
    datasets[0].data = new_data['data']
    chart.update()
  }

  this.reset = function () {
    chart.destroy()
    chart = new Chart(context, { type: "bar", data: data, options: options })
  }
}
