import numpy as np
from mesa.visualization.ModularVisualization import VisualizationElement


class MortgageHistogramModule(VisualizationElement):
    package_includes = ["Chart.min.js"]
    local_includes = ["visualization/HistogramModule.js"]

    def __init__(self, label, bins, canvas_height, canvas_width):
        self.canvas_height = canvas_height
        self.canvas_width = canvas_width
        self.bins = bins
        self.bins_range = None
        new_element = f'new HistogramModule("{label}", {bins}, "{canvas_width}", "{canvas_height}")'
        self.js_code = f'elements.push({new_element});'

    def render(self, model):
        series = [household.wtp_sale() for household in model.households_without_homes]
        lowest_value = min(series)
        self.bins_range = [lowest_value, 2e6]
        hist = np.histogram(series, bins=self.bins, range=self.bins_range)
        
        return {'labels': [int(x) for x in hist[1]],
                'data': [int(x) for x in hist[0]]}