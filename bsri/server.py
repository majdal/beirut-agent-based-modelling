from mesa.visualization.modules import CanvasGrid, TextElement, ChartModule
from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.UserParam import UserSettableParameter

from model import BSRIModel
from agents import HousingUnitAgent, HouseholdAgent, InvestorAgent
from visualization.housing_budget_histogram import HousingBudgetHistogramModule
from visualization.asking_prices_rent_histogram import AskingPricesRentHistogramModule
from visualization.asking_prices_sale_histogram import AskingPricesSaleHistogramModule
from visualization.mortgages_histogram import MortgageHistogramModule

import scipy.interpolate as interpolate
from colour import Color

scale = interpolate.interp1d([0,1], [10, 90])
distance_scale = interpolate.interp1d([0.9,1.0], [10, 90])
price_scale = interpolate.interp1d([1e3,11e3], [10, 100], bounds_error=False, fill_value=(10,100))

yellow = 'hsla(59, 70%, 40%, 1)'
green = 'hsla(145, 70%, 40%, 1)'
blue = 'hsla(214, 70%, 40%, 1)'
grey = 'hsla(0, 0%, 85%, 1)'

price_red = Color('red')
black = Color('black')
price_green = Color('green')
price_colour_scale_value = interpolate.interp1d([0,10e3], [0, 9], bounds_error=False, fill_value=(0,9))
# price_colour_scale_value = interpolate.interp1d([0,11e3], [10, 90], bounds_error=False, fill_value=(10,90))
price_colour_scale = list(price_green.range_to(price_red, 10))

def price_color(price):
    price_colour_index = int(price_colour_scale_value(price))
    return price_colour_scale[price_colour_index].get_hex()


grid_rows = 100
grid_cols = 100


class InvestorsElement(TextElement):
    """ Counts the number of investors
    """

    def render(self, model):
        percent = lambda n: round(n*100, 2)
        investors = model.schedule.get_breed_agents(InvestorAgent)
        corporates = [investor for investor in investors if investor.category == 'corporate']
        corporates_rate = percent(len(corporates)/len(investors))
        investments = [housing_unit for housing_unit in model.schedule.get_breed_agents(HousingUnitAgent) if type(housing_unit.owner) == InvestorAgent]
        corporate_investments = [housing_unit for housing_unit in investments if housing_unit.owner.category == 'corporate']
        corporate_investments_rate  = percent(len(corporate_investments)/len(investments))
        vacancy_rate = percent(len(model.vacancies)/model.n_housing_units)
        return f'Vacancy rate is {vacancy_rate}%. <br/> {corporates_rate}% of investors are corporate, and they own {corporate_investments_rate}% of investment units'


class HouseholdsElement(TextElement):
    """ Gives info about households
    """

    def render(self, model):
        households = model.schedule.get_breed_agents(HouseholdAgent)
        # households_w_budget = [household for household in households if household.budget is not None]
        return f'households: {len(households)}'


def city_portrayal(agent):
    """ A canvas grid portrayal, showing the ownership distribution housing units
    """
    assert agent is not None
    portrayal = {"Shape": "rect", "w": 1, "h": 1, "Filled": "true", "text_color": 'black'}
    portrayal["Layer"] = 0
    portrayal["x"] = agent.pos[0]
    portrayal["y"] = agent.pos[1]
    portrayal["pos"] = agent.pos
    portrayal["neighbourhood"] = agent.neighbourhood
    portrayal["area"] = f'{round(agent.area,1)} m^2'
    portrayal["tenure type"] = agent.tenure_type
    portrayal["owner"] = str(agent.owner)
    portrayal["tenant"] = str(agent.tenant)
    portrayal["on market"] = agent.on_market
    if agent.on_market == 'sale':
        # if it's on the sale market
        # dark green
        color = 'hsla(145, 70%, 40%, 1)'
    elif agent.on_market == 'rent':
        # if it's on the rental market 
        # dark blue
        color = 'hsla(214, 70%, 40%, 1)'
    elif agent.tenure_type == 'investment' and agent.tenant:
        # if it's rented out currently
        # unsaturated blue
        color = 'hsla(214, 60%, 80%, 1)'
    elif agent.is_vacant and not (agent.for_sale or agent.for_rent):
        # if it's an investment off the market 
        # yellow
        color = 'hsla(50, 86%, 80%, 1)' 
    else:
        # if it's owned by a household that lives in it
        # grey
        color = 'hsla(0, 0%, 85%, 1)'
    # portrayal["Color"] = blue if agent.on_market == 'rent' else green if agent.on_market == 'sale' else grey
    portrayal["Color"] = color
    portrayal["price"] = f'sale = ${round(agent.price, 0)}, rent = {round(agent.price_rental, 0)}'
    portrayal["steps in market"] = int(agent.steps_in_market)
    portrayal["steps occupied"] = int(agent.steps_occupied)
    # portrayal["text"] = '-' if agent.tenure_type == 'investment' and agent.tenant else '' if agent.is_vacant and not (agent.for_sale or agent.for_rent) else ''
    # portrayal["text"] = '.' if agent.tenure_type == 'investment' and agent.tenant else '' if agent.is_vacant and not (agent.for_sale or agent.for_rent) else ''
    portrayal["text"] = '.' if agent.tenure_type == 'investment' and agent.owner.category == 'corporate' else ''
    portrayal["price trend sale"] = f'avg={int(agent.realtor.price_trend_sale["average"])}, trend={round(agent.realtor.price_trend_sale["trend"], 3)}'
    portrayal["price trend rent"] = f'avg={int(agent.realtor.price_trend_rent["average"])}, trend={round(agent.realtor.price_trend_rent["trend"], 3)}'
    portrayal["asking price sale"] = int(agent.asking_price_sale)
    portrayal["asking price rent"] = int(agent.asking_price_rent)

    return portrayal

def realtor_portrayal(agent):
    """ shows the distribution average prices per neighbourhood
    """
    assert agent is not None
    portrayal = {"Shape": "rect", "w": 1, "h": 1, "Filled": "true", "text_color": 'black'}
    portrayal["Layer"] = 1
    portrayal["Color"] = price_color(agent.realtor.price_trend_sale['average'])
    portrayal["neighbourhood"] = agent.neighbourhood
    portrayal["housing units count"] = len(agent.realtor.all_housing_units)
    portrayal["price trend sale"] = f'avg={int(agent.realtor.price_trend_sale["average"])}, trend={round(agent.realtor.price_trend_sale["trend"], 3)}'
    portrayal["price trend rent"] = f'avg={int(agent.realtor.price_trend_rent["average"])}, trend={round(agent.realtor.price_trend_rent["trend"], 3)}'

    return portrayal


city_portrayal_element = CanvasGrid(city_portrayal, grid_rows, grid_cols)
realtor_portrayal_element = CanvasGrid(realtor_portrayal, grid_rows, grid_cols)
investors_element = InvestorsElement()
households_element = HouseholdsElement()
seeking_tenants = ChartModule([{"Label": "unhoused tenants", "Color": 'black'},
                               {"Label": "vacancies", "Color": 'red'},
                               {"Label": "step departures", "Color": '#7B68EE'}],
                              data_collector_name='datacollector')
for_rent_or_sale = ChartModule([
                                {"Label": "vacancy (off market)", "Color": 'hsla(50, 86%, 80%, 1)' },
                                {"Label": "vacancy (for sale)", "Color": 'hsla(145, 70%, 40%, 1)'},
                                {"Label": "vacancy (for rent)", "Color": 'hsla(214, 70%, 40%, 1)'},
                                {"Label": "vacancy total", "Color": 'hsla(252, 65%, 50%, 1)'},
                                ],
                               data_collector_name='datacollector')

rental_budgets = HousingBudgetHistogramModule('Montly housing budget', 80, '100%', 300)
asking_prices_rent = AskingPricesRentHistogramModule('Rental asking prices', 40, '100%', 300)
mortgage_size = MortgageHistogramModule('Mortgage carrying capacity', 80, '100%', 300)
asking_prices_sale = AskingPricesSaleHistogramModule('Sale asking prices', 40, '100%', 300)

model_params = {
    'height': grid_rows,
    'width': grid_cols,
    
    # City parameters
    'city parameters section': UserSettableParameter('static_text', value='City parameters'),
    'central_bank_interest_rate': UserSettableParameter('slider', 'Interest rate at from central bank bonds', value=0.1, min_value=0.0, max_value=0.20, step=0.001),
    'tax_rate_vacancy': UserSettableParameter('slider', 'Vacancy tax rate', value=0.01, min_value=0.0, max_value=0.05, step=0.001),
    'tax_rate_property_vacant': UserSettableParameter('slider', 'Property tax rate for vacant properties', value=0.01, min_value=0.0, max_value=0.05, step=0.001),
    'tax_rate_property': UserSettableParameter('slider', 'Property tax rate for occupied properties', value=0.01, min_value=0.0, max_value=0.05, step=0.001),
    'tax_rate_transfer': UserSettableParameter('slider', 'Transfer tax rate', value=0.01, min_value=0.0, max_value=0.05, step=0.001),
    'new_households_per_1k_units_per_step': UserSettableParameter('slider', 'New households per thousand housing units per step', value=9.5, min_value=0, max_value=30, step=0.5),
    'wealth_distribution_shape': UserSettableParameter('slider', 'Income distribution (higher = more poor people)', value=1.11, min_value=0.4, max_value=2, step=0.05),
    'renter_buyer_ratio': UserSettableParameter('slider', 'Renter to buyer ratio (1 = all buyers)', value=1/3, min_value=0.0, max_value=1, step=0.01),
    'minimum_rent_contract_length':  UserSettableParameter('slider', 'Minimum length of rental contract', value=3, min_value=1, max_value=20, step=1),
    'force_average_prices_down': UserSettableParameter('checkbox', 'Force prices down if market is static', value=False),
    
    # Household specific parameters
    'household parameters section': UserSettableParameter('static_text', value='Household parameters'),
    'interest_rate_households': UserSettableParameter('slider', 'Household average interest rate', value=0.05, min_value=0.01, max_value=0.1, step=0.01),
    'minimum_household_budget': UserSettableParameter('slider', 'Minimum household budget', value=600, min_value=400, max_value=1000, step=10),
    'household_pref_amenities': UserSettableParameter('slider', 'Stregth of preference for amenities (instead of distance)', value=0.5, min_value=0, max_value=1, step=0.05),
    'household_units_to_sample': UserSettableParameter('slider', 'Average housing units to view while seeking', value=10, min_value=1, max_value=20, step=1),
    
    # Corporate investor parameters
    'corporate investor parameters section': UserSettableParameter('static_text', value='Corporate investor parameters'),
    'investor_corporate_interest_rate': UserSettableParameter('slider', 'Corporate average interest rate', value=0.09, min_value=0.01, max_value=0.15, step=0.01),
    'investor_corporate_mortgage_share': UserSettableParameter('slider', 'Corporate average loan share of investment', value=0.7, min_value=0.00, max_value=0.7, step=0.05),
    'investor_corporate_discount_factor': UserSettableParameter('slider', 'Corporate discount factor (impatience)', value=0.9, min_value=0.5, max_value=1, step=0.05),
    'investor_corporate_loss_aversion': UserSettableParameter('slider', 'Corporate average loss aversion', value=1, min_value=0.5, max_value=3, step=0.05),
    'investor_corporate_probability_weight': UserSettableParameter('slider', 'Corporate probability weight (market knowledge)', value=0.8, min_value=0.5, max_value=1, step=0.02),
    
    # Amateur investor parameters
    'amateur investor parameters section': UserSettableParameter('static_text', value='Amateur investor parameters'),
    'investor_amateur_interest_rate': UserSettableParameter('slider', 'Amateur average interest rate', value=0.11, min_value=0.01, max_value=0.15, step=0.01),
    'investor_amateur_mortgage_share': UserSettableParameter('slider', 'Amateur average loan share of investment', value=0.1, min_value=0.00, max_value=0.7, step=0.05),
    'investor_amateur_discount_factor': UserSettableParameter('slider', 'Amateur discount factor (impatience)', value=0.8, min_value=0.5, max_value=1, step=0.05),
    'investor_amateur_loss_aversion': UserSettableParameter('slider', 'Amateur average loss aversion', value=2.25, min_value=0.5, max_value=3, step=0.05),
    'investor_amateur_probability_weight': UserSettableParameter('slider', 'Amateur probability weight (market knowledge)', value=0.7, min_value=0.5, max_value=1, step=0.02),
    
    # Experiment logistics parameters
    'Experiment logistics parameters section': UserSettableParameter('static_text', value='Experiment logistics'),
    'save_data': UserSettableParameter('checkbox', 'Export data to Python pickles after run is complete', value=False),
    'export_geojson': UserSettableParameter('checkbox', 'Export data to GeoJSON', value=False),
}

server = ModularServer(
    BSRIModel,
    [city_portrayal_element,
    investors_element,
    for_rent_or_sale,
    seeking_tenants,
    rental_budgets, 
    asking_prices_rent,
    mortgage_size,
    asking_prices_sale,
    realtor_portrayal_element,
    ],
    "Buy/Sell/Rent/Invest (BSRI) model",
    model_params,
)
