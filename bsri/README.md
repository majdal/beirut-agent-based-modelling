# BSRI model 

This ABM is an attempt to create a Buy/Sell, Rent, Invest model in a simulated 
urban environment. It's a contribution towards my (Majd Al-Shihabi) master's 
thesis in Urban Planning and Policy at the American University of Beirut.

## Model details

We have two types of agents (so far): 

1. HousingUnitAgent
2. HouseholdAgent
3. InvestorAgent 
