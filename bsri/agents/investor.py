import math

from mesa import Agent
import scipy.interpolate as interpolate

scale = interpolate.interp1d([0.5,3.0], [0.90, 1.0], bounds_error=False, fill_value=(0.9, 1.0))

# For now, only accept two types of investors: corporate and amateur
# Eventually add 'professional' and 'rising' categories
INVESTOR_CATEGORIES = ['corporate', 'amateur', ]


class InvestorAgent(Agent):
    """ An investor owns multiple apartments in the city. Most likely a developer

    Use the classification system developed by Mneimneh (2019). 

    alpha, beta, gamma, and lambda are parameters from prospect theory. 
        Those need to be tuned to the categories of investors, based on future research. 

    How do the two types of investors differ? 
    Corporate developer: 
        Owns many units
        has more access to financial institutions -> lower interest rate, higher mortgages share 
        more institutional structure -> lower risk aversion, less impatient (higher discount factor)
        more knowledge of the market -> accurate estimation of market trends (high gamma)
    Amateur developer: 
        Owns only 1 unit
        has less access to financial institutions -> higher interest rate, pre-sells unit, has lower mortgage share
        more institutional structure -> high risk aversion, more impatient (lower discount factor)
        less knowledge of the market -> more suseptible to anecdotal evidence (low gamma)

    # TODO add expat (مغترب) investor? 
    
    interest_rate and mortgage_share excepress the degree of financialization of a specific developer/investor, 
        or their access to financial instruments and institutions that facilitate their processes
    
    alpha and beta are fixed because we don't need that much nuance, and definitely don't have granular enough data for it to matter

    Properties: 
        alpha: risk-attitude parameter for gains 
        beta: risk-attitude parameter for losses
        gamma: amount of over or under weighting in the weighting function. 
            Interpretation: amount of knowledge about the market, or how influenced is an investor by anecdotal evidence. 
    """

    def __init__(self, unique_id, model, 
                 category, 
                 units_owned=set(),
                 optimism_factor=None,
                 a_lambda=None,
                 interest_rate=None,
                 mortgage_share=None,
                 discount_factor=None):
        """
        Instatiate a new investor agent. 

        Args: 
            category: must be either 'corporate' or 'amateur'. Must be used in conjunction 
            discount_factor: factor of the present value of future utility. 
                             Amateur developers are more interested in current utitliy, and thus have a higher discount factor 
                             Interpretation: measurement of impatience
            optimism_factor: how much does an investor overshoot the market rate in their willingness to ask for rent or sale prices
            a_lambda: loss-aversion coefficient (prospect theory)
            interest_rate: the interest rate that the investor pays for their loans
            mortgage_share: 
        """
        super().__init__(unique_id, model)
        assert category in INVESTOR_CATEGORIES, 'Category is not among possible categories'
        self.category = category
        self.housing_units_owned = units_owned
        self.optimism_factor = optimism_factor or self.random.gauss(1, 0.01)
        self.optimism_factor_backup = self.optimism_factor
        self.alpha = 1
        self.beta = 1
        self.phi = self.random.gauss(10, 2)
        # Need to differentiate between corporate and amateur investors. They have different risk tolerance, and different outlook on the future
        if category == 'corporate':
            self.a_lambda = a_lambda or abs(self.random.gauss(model.investor_corporate_loss_aversion, 0.065))
            self.gamma = model.investor_corporate_probability_weight
            self.discount_factor = discount_factor or model.investor_corporate_discount_factor
            self.mortgage_share = mortgage_share or abs(self.random.gauss(model.investor_corporate_mortgage_share, 0.01))
            self.interest_rate = interest_rate or abs(self.random.gauss(model.investor_corporate_interest_rate, 0.001))
        elif category == 'amateur':
            self.a_lambda = a_lambda or abs(self.random.gauss(model.investor_amateur_loss_aversion, 0.065))
            self.gamma = model.investor_amateur_probability_weight
            self.discount_factor = discount_factor or model.investor_amateur_discount_factor
            self.mortgage_share = mortgage_share or abs(self.random.gauss(model.investor_amateur_mortgage_share, 0.01))
            self.interest_rate = interest_rate or abs(self.random.gauss(model.investor_amateur_interest_rate, 0.001))

    def __str__(self):
        return f'Investor ({self.category}): {self.unique_id}'

    def __repr__(self):
        return f'Investor {self.unique_id}: {self.category}'

    @property
    def vacant_units_owned(self):
        return [unit for unit in self.housing_units_owned if unit.is_vacant]

    def formulate_asking_price_rent(self, unit):
        average_and_trend = unit.realtor.price_trend_rent
        average_price_neighbourhood = average_and_trend['average']
        trend_neighbourhood = 1 + average_and_trend['trend']
        list_price = average_price_neighbourhood*unit.area * \
                     trend_neighbourhood * \
                     self.optimism_factor  # * \
                     # unit.steps_in_market
                     # TODO how do we operationalize steps in market?
        return list_price

    def formulate_asking_price_sale(self, unit):
        average_and_trend = unit.realtor.price_trend_sale
        average_price_neighbourhood = average_and_trend['average']
        trend_neighbourhood = 1 + average_and_trend['trend']
        list_price = average_price_neighbourhood*unit.area * \
                     trend_neighbourhood * \
                     self.optimism_factor  # * \
                     # unit.steps_in_market
        return list_price

    def rate_of_return(self, housing_unit, mode):
        """ Calculate rate of return, which is gain/down payment (V/D)
        all values normalized to 1 time step (1 year) 

        V = capital_gain - interest_due + rent - operating_cost
        D = Down payment

        Args: 
            housing_unit: the housing unit that we're calculating the RoR for 
            mode: one of 'rent', 'sell', or 'wait'

        TODO We can easily add a use-value, U in place of rent for expatriate owners to represent using the property - say one month a year - when they are not renting the property.
        """
        assert mode in ['rent', 'sell', 'wait'], "Enter one of  'rent', 'sell', or 'wait'"
        
        if mode == 'rent':
            asking_rent = housing_unit.asking_price_rent
            rent_share_of_price = (asking_rent*self.model.months_in_timestep) / housing_unit.price 
            operating_cost_share_of_price = (housing_unit.operating_costs*self.model.months_in_timestep) / housing_unit.price
            taxes = self.model.tax_rate_property
        elif mode == 'sell':
            rent_share_of_price = 0
            operating_cost_share_of_price = 0
            taxes = self.model.tax_rate_transfer
        else: # if we're choosing to wait
            rent_share_of_price = 0
            operating_cost_share_of_price = (self.model.operating_costs_vacant*housing_unit.operating_costs*self.model.months_in_timestep) / housing_unit.price
            taxes = self.model.tax_rate_vacancy + self.model.tax_rate_property_vacant
        
        expected_price_sale = self.formulate_asking_price_sale(housing_unit)
        expected_price_increase = (expected_price_sale - housing_unit.price) / housing_unit.price

        mortgage_share_of_price = self.mortgage_share

        interest_rate = self.interest_rate
        interest_due = (1 + interest_rate)*mortgage_share_of_price
        
        rate = (self.discount_factor*(1 + expected_price_increase - interest_due) + rent_share_of_price - operating_cost_share_of_price - taxes) / \
                    (1 - mortgage_share_of_price)
        return rate

    def calculate_ptv(self, gamble):
        """ Prospect Theory Value
        takes a list of tuples of outcomes and their chances, i.e.: 
        [(outcome_1, probability_1), (outcome_2, probability_2), ...]
        returns an ptv value using the investor's own value of alpha, beta, gamma        
        """
        def v(x): 
            return x**self.alpha if x > 0 else - self.a_lambda*((-1*x)**self.beta)
        def w(p): 
            return (p**self.gamma) / ((p**self.gamma+(1-p)**self.gamma)**(1/self.gamma))

        ptv = sum(map(lambda pair: v(pair[0])*w(pair[1]), gamble))
        return ptv

    def compare_choices(self, choice_a, choice_b):
        probability = (1+(math.e)**(self.phi*(choice_b - choice_a)))**(-1)
        return probability

    def calculate_prospects(self, housing_unit):
        """ Calculate prospects for each of the three prospects, while adjusting the outcomes based on the market conditions
        """
        housing_unit.asking_price_sale = self.formulate_asking_price_sale(housing_unit)
        housing_unit.asking_price_rent = self.formulate_asking_price_rent(housing_unit)
        ror_rent = self.rate_of_return(housing_unit, mode='rent')
        # ror_sale = self.rate_of_return(housing_unit, mode='sell')
        ror_wait = self.rate_of_return(housing_unit, mode='wait')
        ror_alt = 1 + self.model.central_bank_interest_rate

        probability_rent = housing_unit.realtor.probability_rent(housing_unit.asking_price_rent)
        # probability_sale = housing_unit.realtor.probability_sale(housing_unit.asking_price_sale)

        rent_gamble = [(ror_rent, probability_rent), (ror_wait, 1-probability_rent)]
        # sell_gamble = [(ror_sale, probability_sale), (ror_wait, 1-probability_sale)]
        wait_gamble = [(ror_wait, 1), ]
        alt_gamble = [(ror_alt, 0.99), (-0.6, 0.01)] # 99% chance of getting central bank rate of return, and 1% chance of getting a 60% haircut
        # the value of probability of getting the alt gamble is the country's credit rating?

        prospects = dict()
        prospects['rent'] = self.calculate_ptv(rent_gamble)
        # prospects['sell'] = self.calculate_ptv(sell_gamble)
        prospects['wait'] = self.calculate_ptv(wait_gamble)
        prospects['alt'] = self.calculate_ptv(alt_gamble)
        ptv_choice = max(prospects, key=prospects.get)        


        choice = None
        if   prospects['wait'] <= prospects['alt'] and prospects['alt'] <= prospects['rent']:
            choice = 'rent'
        elif prospects['rent'] <= prospects['alt'] and prospects['wait'] <= prospects['alt']:
            choice = 'sell'
        elif prospects['rent'] <= prospects['alt'] and prospects['alt'] <= prospects['wait']:
            choice = 'wait'
        elif prospects['alt'] <= prospects['wait'] and prospects['alt'] <= prospects['rent']:
            choice = 'wait' if prospects['wait'] > prospects['rent'] else 'rent'
        prospects['choice'] = choice

        record = {'investor id': self.unique_id,
                  'step': self.model.schedule.steps,
                  'gamma': self.gamma,
                  'lambda': self.a_lambda,
                  'discount factor': self.discount_factor,
                  'optimism factor': self.optimism_factor,
                  'interest rate': self.interest_rate,
                  'mortgage share': self.mortgage_share,
                  'operating costs': int(housing_unit.operating_costs),
                  'tax vacancy': self.model.tax_rate_vacancy,
                  'tax property': self.model.tax_rate_property,
                  'tax property vacant': self.model.tax_rate_property_vacant,
                  'tax transfer': self.model.tax_rate_transfer,
                  'price': int(housing_unit.price),
                  'rent': int(housing_unit.price_rental),
                  'asking price rent': int(housing_unit.asking_price_rent),
                  'asking price sale': int(housing_unit.asking_price_sale),
                  'area': housing_unit.area,
                  'neighbourhood': housing_unit.neighbourhood,
                  'neigbhourhood average sale': housing_unit.realtor.price_trend_sale['average'],
                  'neigbhourhood trend sale': housing_unit.realtor.price_trend_sale['trend'],
                  'neigbhourhood average history rent': list(map(lambda n: int(n), housing_unit.realtor.average_house_price_rent[-3:])),
                  'neigbhourhood average history sale': list(map(lambda n: int(n), housing_unit.realtor.average_house_price_sale[-3:])),
                #   'ror sale': ror_sale,
                  'ror rent': ror_rent,
                  'ror wait': ror_wait, 
                #   'prob sale': probability_sale,
                  'prob rent': probability_rent,
                #   'ptv sale': prospects['sell'],
                  'ptv rent': prospects['rent'],
                  'ptv wait': prospects['wait'],
                  'ptv alt': prospects['alt'],
                  'choice': choice,
                  'ptv choice': ptv_choice}
        # self.model.datacollector.add_table_row('prospects', record, ignore_missing=True)
        return prospects

    def settle_bids_sale(self, unit):
        """
        make a match between bidders and the investor. Follows a satisficing algorithm
        """
        price_trend = unit.realtor.price_trend_sale
        average_price = unit.area * (1+price_trend['average'])
        lowest_price = average_price * scale(self.a_lambda)
        bids_evaluated = 0
        for bid in unit.bids_sale:
            # print(f'evaluating bids for {unit}')
            if not bid['bidder'].withdraw_bids:
                if bid['wtp'] > unit.asking_price_sale:
                    bids_evaluated += 1
                    final_price = min([bid['wtp'], unit.asking_price_sale*1.2])
                    unit.realtor.register_trade(unit, bid['bidder'], final_price)
                    break
                else:
                    bids_evaluated += 1
                    self.optimism_factor -= 0.01 # we're becoming desperate 
                    prospects = self.calculate_prospects(unit)

                    if prospects['choice'] == 'sell':
                        # now it's more profitable for the invetor to rent in the next time step to hedge their losses 
                        if bid['wtp'] > lowest_price:
                            final_price = int((bid['wtp']+lowest_price)/2)
                            unit.realtor.register_trade(unit, bid['bidder'], final_price)
                            break
                    else:
                        break
        # if unit.bids_sale:
        #     print(f'SALE: Evaluated the first {bids_evaluated}/{len(unit.bids_sale)} of bids received ({round((bids_evaluated/len(unit.bids_sale))*100, 2)}%)')

    def settle_bids_rent(self, unit):
        """
        Make a match between tenants and the investor. 
        Bids are evaluated in the order that they arrive in. 
        """
        lowest_price = unit.asking_price_rent * scale(self.a_lambda)
        bids_evaluated = 0
        for bid in unit.bids_rent:
            # print(f'evaluating bids for {unit}')
            if not bid['bidder'].withdraw_bids:
                # Go through the bids in order of receiving them
                if bid['wtp'] > unit.asking_price_rent:
                    bids_evaluated += 1
                    final_price = min([bid['wtp'], unit.asking_price_rent*1.2]) # don't blow up the price record
                    unit.realtor.register_rent(unit, bid['bidder'], final_price)
                    break
                elif bid['wtp'] > lowest_price:
                    bids_evaluated += 1
                    final_price = int((bid['wtp']+lowest_price)/2)
                    unit.realtor.register_rent(unit, bid['bidder'], final_price)
                    break
        # if unit.bids_rent:
        #     print(f'RENT: Evaluated the first {bids_evaluated}/{len(unit.bids_rent)} of bids received ({round((bids_evaluated/len(unit.bids_rent))*100, 2)}%)')

    def evict_tenant(self, unit):
        # print(f'{self} evicted {unit.tenant}')
        unit.steps_occupied = 0
        unit.tenant.housed_at = None
        unit.tenant = None

    def step(self):
        # reset optimism factor 
        self.optimism_factor = self.optimism_factor_backup
        for unit in self.housing_units_owned:
            if unit.tenant:
                # reset the rental contract if applicable
                if unit.steps_occupied > self.model.minimum_rent_contract_length and \
                   self.model.allow_evictions and \
                   unit.realtor.price_trend_rent['trend'] > 0 and \
                   self.random.random() < self.model.probability_eviction:
                        # If a household has been living in a housing unit longer than the contract period,
                        # and the market trend is positive, the contract is likely revaluated 
                        # TODO chance of revaluation probably should be dependant on the type of investor 
                    self.evict_tenant(unit)
            else:
                prospects = self.calculate_prospects(unit)
                
                choice = prospects['choice']

                if choice == 'rent':
                    unit.for_rent = True
                    unit.for_sale = False
                elif choice == 'sell':
                    unit.for_rent = False
                    unit.for_sale = True
                else:
                    unit.for_rent = False
                    unit.for_sale = False
        # TODO what if the two prospect values are close? Can we set a tolerance? what's the tolerance of the isclose function? adjust based on the order of magnitude of the prospects
        # if math.isclose(self.prospects['rent'], self.prospects['sell'], abs_tol=1e-5):