from mesa import Agent


POSSIBLE_BREEDS = ['tenant', 'owner', 'buyer', ]


class HouseholdAgent(Agent):
    """ An agent that is buying/selling, renting, or investing. 

    An agent can have one of 3 possible breeds shift between breeds as the simulation proceeds. 
    Possible breeds are: 
    - tenant: live in a rented apartment
    - owner: live in an apartment they own
    - buyer: they are seeking an apartment to buy 

    TODO: Additional attributes for a household (for future research):
    - propensity to move: a factor of how much of a price increase will a household endure before they seek alternative housing
    - sectarian affiliation: will integrating this into the utility function or the unit sampling function affect 
    - does a household's budget change over time?
    - how many units does a household sample when they are seeking housing? Default now is 10, but should be investigated
    - neighbourhood preferene: do household prefer very specific neighbourhoods? so the sample of units is within a geographic region?
    - richer households probably have a larger downpayment, how can we reflect that?
    - is there a different utility function for sale vs rent? 

    Properties: 
        breed: defines the behaviour of the agent at each step. 
        budget: how much budget does this agent have *for housing* on a monthly basis, not including transportation. 
        interest_rate: what interest rate does a household have access to? 
        downpayment: how much % of the mortgage can a household afford to increase their willingness to pay?
                     This is a percentage [0,1], not an absolute number!
        housed_at: a reference to the HousingUnitAgent where the agent is housed
        pref_proximity_aub: the prefernce to proximity to this location in the city. The distance preferences should add up to 0.5
        pref_proximity_usj: 
        pref_proximity_lau: 
        pref_proximity_centre: 
        pref_amenities: the preference for amenities, e.g. building quality, reception, etc...
    """

    def __init__(self, unique_id, model,
                 breed,
                 budget=None,
                 interest_rate=0.05,
                 downpayment=0.3,
                 pref_proximity_aub=0.125,
                 pref_proximity_lau=0.125,
                 pref_proximity_usj=0.125,
                 pref_proximity_centre=0.125,
                 pref_amenities=None,
                 units_to_sample=None,
                 housed_at=None):
        super().__init__(unique_id, model)
        assert breed in POSSIBLE_BREEDS, 'Breed not among possible breeds'
        self.breed = breed
        self.budget = budget or (model.minimum_household_budget * self.random.paretovariate(model.wealth_distribution_shape))
        self.interest_rate = interest_rate or self.model.interest_rate_households
        self.downpayment = downpayment
        self.pref_proximity_aub = pref_proximity_aub
        self.pref_proximity_usj = pref_proximity_usj
        self.pref_proximity_lau = pref_proximity_lau
        self.pref_proximity_centre = pref_proximity_centre
        self.pref_amenities = pref_amenities or abs(self.random.gauss(model.household_pref_amenities, 0.02)) # TODO change this to a beta distribution 
        self.units_to_sample = units_to_sample or abs(int(self.random.gauss(model.household_units_to_sample, 2)))
        self._housed_at = housed_at
        self.steps_in_search = 0
        self.withdraw_bids = False

    def __str__(self):
        return f'Household ({self.breed}): {self.unique_id}'

    def __repr__(self):
        return f'Household {self.unique_id}: {self.breed}'

    @property
    def is_housed(self):
        return True if self.housed_at else False

    @property
    def steps_in_current_unit(self):
        return self.housed_at.steps_occupied
    
    @steps_in_current_unit.setter
    def steps_in_current_unit(self, n_steps):
        self.housed_at.steps_occupied = n_steps

    @property
    def housed_at(self):
        return self._housed_at
    
    @housed_at.setter
    def housed_at(self, housing_unit):
        if self.breed == 'owner' and housing_unit is None:
            raise AttributeError
        self._housed_at = housing_unit

    def switch_to_investment(self):
        """ A household decides to either sell or rent out their own unit, thus becoming an investor.
        Replaces the owner with a new amateur investor that owns only that particular unit, 
        with a low discount factor, meaning that they want to cash out as quickly as possible. 
        """
        self.model.step_flipped_units.append(self.housed_at.unique_id)
        investor_type = 'amateur' if self.random.random() < 0.2 else 'corporate'
        investor = self.model.add_investor(investor_type)
        # print(f'owner {self} flipped {self.housed_at.unique_id} to investment by {investor}')
        # investor.discount_factor = 0.5
        investor.housing_units_owned = {self.housed_at}
        investor.mortgage_share = 0.5 #max([self.housed_at.steps_occupied/self.model.default_mortgage_period - self.downpayment, 0])
        self.housed_at.owner = investor
        self.housed_at.steps_occupied = 0
        self.housed_at.tenant = None
        self.housed_at.steps_in_market = 0
        self.model.schedule.remove(self)

    def get_utility(self, housing_unit):
        """ A Cobbs-Douglas function that integrates a household's preferences
        Used to prioritize the bids
        """
        utility = (housing_unit.normalized_proximity_aub**self.pref_proximity_aub) * \
                  (housing_unit.normalized_proximity_usj**self.pref_proximity_usj) * \
                  (housing_unit.normalized_proximity_lau**self.pref_proximity_lau) * \
                  (housing_unit.normalized_proximity_centre**self.pref_proximity_centre) * \
                  (housing_unit.amenities**self.pref_amenities)
        return utility

    def wtp_rent(self, housing_unit=None):
        """ Willingness to Pay function, housing unit is optional. 
        Right now, the wtp is exactly the same as budget, but maybe this should be flexible 
            to includ market conditions, utility preference, etc...
        """
        bid = self.budget
        if housing_unit:
            utility = self.get_utility(housing_unit)
        if housing_unit:
            if bid > housing_unit.asking_price_rent*2:
                # don't overbid on a housing unit 
                bid = housing_unit.asking_price_rent * 1.2
        return bid 

    def wtp_sale(self, period=None, housing_unit=None):
        """ what's the size of the mortgage that a household can afford? 
        Based on the monthly budget, and downpayment

        args: 
            period: the period of the mortgage in timesteps (aka years) if not defined, it's the global period defined at model init
        """
        period = period or self.model.default_mortgage_period
        period_months = period*self.model.months_in_timestep
        monthly_interest = self.interest_rate/12
        mortgage_size = self.budget * ((1+monthly_interest)**period_months - 1) / (monthly_interest*(1+monthly_interest)**period_months)
        if housing_unit:
            utility = self.get_utility(housing_unit)
            # currently utility is not used, in wtp calculation. But maybe in the future it would? 
        total_mortgage = mortgage_size * (1 + self.downpayment)
        if housing_unit:
            if total_mortgage > housing_unit.asking_price_sale*2:
                # don't overbid on a housing unit 
                total_mortgage = housing_unit.asking_price_sale*1.2
        return total_mortgage

    def record_residency(self):
        if self.is_housed:
            residency_record = {
                'id': self.unique_id, 
                'step': self.model.schedule.steps,
                'budget': self.budget, 
                'tenure': self.breed,
                'home': self.housed_at.pos,
                'investor type': 'household' if self.housed_at.tenure_type == 'ownership' else self.housed_at.owner.category,
            }
            self.model.datacollector.add_table_row('residents', residency_record)

    def record_arrival(self): 
        arrival_record = {
            'id': self.unique_id, 
            'budget': self.budget, 
            'arrived at step': self.model.schedule.steps,
            'units to sample': self.units_to_sample,
        }
        self.model.datacollector.add_table_row('arrivals', arrival_record)

    def leave_city(self):
        """ A household cannot find housing, and so they depart... how sad! 
        Their their right to the city was robed from them :(
        """
        # keep record of Households that cannot afford the city and thus leave
        departure_record = {
            'id': self.unique_id, 
            'budget': self.budget, 
            'departed at step': self.model.schedule.steps,
            'units to sample': self.units_to_sample,
        }
        self.model.departures.append(departure_record)
        self.model.step_departures.append(departure_record)
        self.model.schedule.remove(self)
        self.model.datacollector.add_table_row('departures', departure_record)

    def step(self):
        if self.breed == 'owner':
            assert self.is_housed, f'data inconsistency: household {self.unique_id} is an owner, but does not live in a unit'

        if not self.is_housed and self.steps_in_search >= self.model.steps_in_search_before_leaving:
            self.leave_city()
        else:
            if self.breed == 'tenant':
                if self.is_housed:
                    self.steps_in_current_unit += 1
                    # if the 3 year rental contract is over, leave the housing unit and start seeking
                    if self.steps_in_current_unit > 3:
                        pass # TODO how does a tenant lose their current housing unit? Related to propensity to move
                else:
                    self.steps_in_search += 1
                    # if there are units for rent on the market
                    if self.model.market_rent:
                        # if there are units on the market, pick the ones nearest to my budget
                        budget = self.budget
                        wtp = self.wtp_rent()
                        n_units_to_sample = min([self.units_to_sample, len(self.model.market_rent)])
                        units_to_view = self.model.sample_units_rent(budget, n_units_to_sample)
                        units_with_utility = [(unit, self.get_utility(unit)) for unit in units_to_view]
                        units_with_utility.sort(key=lambda tup: tup[1], reverse=True)
                        # place the bids 
                        self.withdraw_bids = False
                        for unit in units_to_view:
                            if wtp*1.3 > unit.asking_price_rent:
                                # don't bid on housing units that are more than 1.3 more expensive than my budget
                                unit.place_bid_rent({'wtp': wtp, 'bidder': self})

            elif self.breed == 'buyer':
                self.steps_in_search += 1
                if self.model.market_sale:
                    wtp = self.wtp_sale() 
                    n_units_to_sample = min([self.units_to_sample, len(self.model.market_sale)])
                    units_to_view = self.model.sample_units_sale(wtp, n_units_to_sample)
                    units_with_utility = [(unit, self.get_utility(unit)) for unit in units_to_view]
                    units_with_utility.sort(key=lambda tup: tup[1], reverse=True)
                    # place sale bid
                    self.withdraw_bids = False
                    for unit in units_to_view:
                        if wtp*1.3 > unit.asking_price_sale:
                            # don't bid on housing units that are more than 1.3 more expensive than my budget
                            unit.place_bid_sale({'wtp': wtp, 'bidder': self})

            elif self.breed == 'owner':
                self.steps_in_current_unit += 1
                if self.steps_in_current_unit > 8 and self.random.random() < 0.01:
                    # TODO the 0.01 is speculative. Find data to see how many housing units are sold per year by households
                    # there's probably a better way to set this condition, but it's ok for now
                    if self.model.only_switch_in_good_trend and self.housed_at.realtor.price_trend_sale['trend'] > 0:
                        self.switch_to_investment()
                    else:
                        self.switch_to_investment()
