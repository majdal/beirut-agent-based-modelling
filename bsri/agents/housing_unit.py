from mesa import Agent


class HousingUnitAgent(Agent):
    """ A housing unit that has a fixed position, doesn't move. 

    Properties: 
        neighbourhood: the realtor/neighbourhood that this housing unit is located in 
        price: the price at which the unit was actually sold
        rent: the price at which the unit was actually rented 
        asking_price: the current asking price by the owner, given the market conditions
        price_rental: the annual rent of the unit
        asking_price_rent: the asking price, as opposed to the actual rental price
        condition: the state of the place, values ranging [0,1], 1 being the best
        area: currently assume all units are of the same area, 100m^2
        distance_***: distance from AUB, USJ, LAU, and downtown. Currently uses euclidian distance. 
        amenities: a catch all for all other amenities, e.g. green spaces, view, light, etc... values [0,1]
    """

    def __init__(self, unique_id, model, 
                 neighbourhood, 
                 neighbourhood_id,
                 geometry=None,
                 condition=0.7,
                 operating_costs=None,
                 area=None,
                 price=None, 
                 rent=None, 
                 owner=None,
                 tenant=None,
                 for_rent=False, 
                 for_sale=False,
                 distance_aub=None,
                 distance_lau=None,
                 distance_usj=None,
                 distance_centre=None,
                 amenities=None,
                 steps_in_market=0,
                 steps_occupied=0,
                 purchased_steps_ago=None):
        super().__init__(unique_id, model)
        self.neighbourhood = neighbourhood
        self.neighbourhood_id = neighbourhood_id
        self.geometry = geometry
        self.area = area or int(self.random.gauss(self.realtor.average_unit_area, 10))
        steps_ago = purchased_steps_ago or self.random.choice(range(len(self.realtor.average_house_price_sale))) # pick a year the unit was bought in
        # steps_ago = -1
        self.price = price or self.area*self.realtor.average_house_price_sale[steps_ago]
        self._price_init = self.price
        self.price_rental = rent or self.area*self.realtor.average_house_price_rent[steps_ago]
        self._price_rental_init = self.price_rental
        self.asking_price_sale = self.price
        self.asking_price_rent = self.price_rental
        self.owner = owner
        self.tenant = tenant
        self.for_rent = for_rent
        self.for_sale = for_sale
        self.keep_empty = False # only used for the initialization to save myself some headache 
        # condition is a value [0,1] indicating the quality of the housing unit, 
        # 0 being an uninhabitable unit
        self.condition = condition
        self.operating_costs = operating_costs or self.price * self.model.operating_costs
        # distance metadata 
        # distance to each of the following campuses is also proxy for distance to other city centres (Hamra, Ashrafieh, Qoreitem)
        self.distance_aub = distance_aub
        self.distance_usj = distance_usj
        self.distance_lau = distance_lau
        self.distance_centre = distance_centre
        # Most apaprtments have an amenity score around 0.5
        # self.amenities = amenities or abs(self.random.gauss(0.5, 0.17)) # take the absolute value to evade negative values which cause problems down the line 
        self.amenities = amenities or self.random.betavariate(5, 2)
        
        # other metadata 
        self.steps_in_market = steps_in_market
        self._steps_occupied = steps_occupied
        # bids are lists because order matters in our algorithm 
        self.bids_sale = list()
        self.bids_rent = list()
        self.flipped_at_step = None

    def __str__(self):
        return f'HousingUnit {self.unique_id}:{" 📈" if self.tenure_type == "investment" else ""} {self.area} m^2, ${int(self.price)}, {self.neighbourhood}'

    def __repr__(self):
        return f'HousingUnit {self.unique_id}: {self.area} m^2, ${int(self.price)}'

    @property
    def steps_occupied(self):
        return self._steps_occupied
    
    @steps_occupied.setter
    def steps_occupied(self, n_steps):
        if not self.tenant:
            raise AttributeError('The tenant is not registered...')
        self._steps_occupied = n_steps

    @property
    def normalized_proximity_aub(self):
        return (self.model.max_dist_aub + 1 - self.distance_aub) / self.model.max_proximity_aub

    @property
    def normalized_proximity_lau(self):
        return (self.model.max_dist_lau + 1 - self.distance_lau) / self.model.max_proximity_lau

    @property
    def normalized_proximity_usj(self):
        return (self.model.max_dist_usj + 1 - self.distance_usj) / self.model.max_proximity_usj

    @property
    def normalized_proximity_centre(self):
        return (self.model.max_dist_centre + 1 - self.distance_centre) / self.model.max_proximity_centre

    @property
    def is_vacant(self):
        return False if self.tenant else True

    @property
    def is_occupied(self):
        return True if self.tenant else False 

    @property
    def tenure_type(self):
        # this is a convoluted way of getting the ownership type to avoid circular imports 
        try:
            investor_category = self.owner.category
            return 'investment'
        except:
            return 'ownership'

    @property
    def realtor(self):
        return self.model.realtors[self.neighbourhood_id]

    @property
    def on_market(self):
        if self.for_sale:
            return 'sale'
        elif self.for_rent:
            return 'rent'
        elif self.tenant and self.tenure_type == 'investment':
            return 'rented'
        else:
            return 'no'

    def place_bid_rent(self,  bid):
        assert bid['bidder'].breed == 'tenant', 'A non-tenant is placing a rental bid!'
        self.bids_rent.append(bid)

    def place_bid_sale(self,  bid):
        assert bid['bidder'].breed == 'buyer', 'A non-buyer is placing a sale bid!'
        self.bids_sale.append(bid)

    def record_data(self):
        # collect tenure data
        on_market = self.on_market
        tenure_type = self.tenure_type

        if on_market == 'sale':
            tenure = 'sale'
        elif on_market == 'rent':
            tenure = 'rent'
        elif on_market == 'rented':
            tenure = 'rented'
        elif on_market == 'no' and tenure_type == 'investment':
            tenure = 'off market'
        else:
            tenure = 'household home'

        record = {
            'step': self.model.schedule.steps,
            'unit id': self.unique_id,
            'geometry': self.geometry,
            'area': self.area,
            'pos': self.pos,
            'x': self.pos[0],
            'y': self.pos[1],
            'legend': tenure,
            'tenure type': self.tenure_type,
            'investor category': self.owner.category if self.tenure_type ==  'investment' else None,
            'investor id': self.owner.unique_id if self.tenure_type ==  'investment' else None,
            'on market': self.on_market,
            'price rent': self.price_rental,
            'price sale': self.price,
            'price sale init': self._price_init,
            'price rent init': self._price_rental_init,
            'rent delta': ((self.price_rental - self._price_rental_init)/self._price_rental_init)*100,
            'sale delta': ((self.price - self._price_init)/self._price_init)*100,
        }
        self.model.datacollector.add_table_row('housing unit ownership', record)

    def step(self):
        if self.for_rent:
            self.steps_in_market += 1
            self.owner.settle_bids_rent(self)
            
        elif self.for_sale:
            self.steps_in_market += 1
            self.owner.settle_bids_sale(self)
        
        # clear rent and sale bids if any exist 
        self.bids_rent.clear()
        self.bids_sale.clear()