from statistics import mean
import numpy as np
import pandas as pd
from scipy.stats import poisson

from mesa import Agent


class RealtorAgent(Agent):
    """ A realtor agent: an information broker that facilitates transactions 
    Specializes in a specific neighbourhood

    Properties: 
        neighbourhood_average_price_rent: the seed for the neighbourhood average rent price, per square meter
        neighbourhood_average_price_sale: the seed for the neighbourhood average sale price, per square meter
        optimism_factor: for selling above the average price
        market_rent: the units available for rent at any time step
        market_sale: the units available for sale at any time step
    """

    def __init__(self, unique_id, model, 
                 neighbourhood, 
                 neighbourhood_average_price_sale_history,
                 neighbourhood_average_price_rent_history,
                 geometry=None,
                 average_unit_area=140):
        super().__init__(unique_id, model)
        # name of neighbourhood that the realtor manages 
        self.neighbourhood = neighbourhood
        self.geometry = geometry
        self.average_unit_area = average_unit_area
        self.all_housing_units = list()
        # housing_units_sold and _rented are the memory of the realtor.
        # The are used in price expectation formation, and for probability calculation
        self.housing_units_sold = list()
        self.housing_units_rented = list()
        self.average_house_price_sale = neighbourhood_average_price_sale_history
        self.average_house_price_rent = neighbourhood_average_price_rent_history
        
        self.market_sale = set()
        self.market_rent = set()      
        self.units_sold = []
        self.units_rented = []
        self._price_trend_sale = {'average': None, 'std': None, 'trend': None}
        self._price_trend_rent = {'average': None, 'std': None, 'trend': None}
        
        # initialize trends for data integrity 
        self.set_price_trend_sale()
        self.set_price_trend_rent()

    def __str__(self):
        return f'Realtor {self.unique_id}: {self.neighbourhood}'

    def __repr__(self):
        return f'Realtor {self.unique_id}: {self.neighbourhood}'

    def set_average_house_price_sale(self):
        """ The average sale price from the most recent time step, normalized by area
        If no units were sold, set it as equal to the past time step.

        TODO should the number of time steps back in history be configurable? Should the weights?
        """
        if len(self.housing_units_sold[-1]) > 1:
            average = mean([round((trade_record['price']/trade_record['area']),2) for trade_record in self.housing_units_sold[-1]])
            average = mean([average, self.average_house_price_sale[-1]]) # stabilize the price increase, since sample size is small
        else:
            # if in the last time step, no new units were sold 
            average = self.average_house_price_sale[-1]
            average *= (1 + self.price_trend_sale['trend']*0.3)
            if self.model.force_average_prices_down:
                average *= 0.97
        if average > self.average_house_price_sale[-1]*1.21 or average < self.average_house_price_sale[-1]*0.79:
            print(f'SALE average big change warning for {self.neighbourhood}!')
            print(f'new average = {int(average)}')
            print(f'previous trend = {round(self.price_trend_sale["trend"], 4)} ')
            print(f'using {len(self.housing_units_rented[-1])} units to calculate')
            print([round(avg, 4) for avg in self.average_house_price_sale])
        self.average_house_price_sale.append(average)

    def set_average_house_price_rent(self):
        """ The rent price for rent for the most recent time step, normalized by area
        If no units were sold, it's equal to the last step.
        """
        if len(self.housing_units_rented[-1]) > 1:
            average = mean([round(trade_record['price']/trade_record['area'],2) for trade_record in self.housing_units_rented[-1]])
            average = mean([average, self.average_house_price_rent[-1]])  # stabilize the price increase, since sample size is small
        else:
            # if in the last time step, no new units were rented
            average = self.average_house_price_rent[-1]
            average *= (1 + self.price_trend_rent['trend']*0.3)
            if self.model.force_average_prices_down:
                average *= 0.99
        if average > self.average_house_price_rent[-1]*1.21 or average < self.average_house_price_rent[-1]*0.79:
            print(f'RENT average big change warning for {self.neighbourhood}!')
            print(f'new average = {int(average)}')
            print(f'previous trend = {round(self.price_trend_rent["trend"], 4)} ')
            print(f'using {len(self.housing_units_rented[-1])} units to calculate')
            print([round(avg, 4) for avg in self.average_house_price_rent])
        self.average_house_price_rent.append(average)

    @property
    def price_trend_sale(self):
        return self._price_trend_sale

    def set_price_trend_sale(self):
        """ The average price, standard deviation, and trend from the last three time steps, 
        weighted more heavily to the most recent step
        """
        weighted_average = 0.25*self.average_house_price_sale[-3] + \
                           0.25*self.average_house_price_sale[-2] + \
                           0.50*self.average_house_price_sale[-1]
        trend = (self.average_house_price_sale[-1] - self.average_house_price_sale[-3]) / \
                abs(self.average_house_price_sale[-3])
        std = np.std(np.diff(self.average_house_price_sale))
        self._price_trend_sale = {'average': weighted_average,
                                  'trend': trend,
                                  'std': std }

    @property
    def price_trend_rent(self):
        return self._price_trend_rent

    def set_price_trend_rent(self):
        weighted_average = 0.25*self.average_house_price_rent[-3] + \
                           0.25*self.average_house_price_rent[-2] + \
                           0.50*self.average_house_price_rent[-1]
        trend = (self.average_house_price_rent[-1] - self.average_house_price_rent[-3]) / \
                abs(self.average_house_price_rent[-3])
        std = np.std(np.diff(self.average_house_price_rent))
        self._price_trend_rent = {'average': weighted_average,
                                  'trend': trend,
                                  'std': std }

    def probability_rent(self, rent):
        # TODO probably need to factor in area and other attributes while locating simililar units, but for now price is a reasonable enough simplification
        units_df_rent = pd.DataFrame.from_records(list(np.concatenate(self.housing_units_rented).flat))
        similar_units = units_df_rent.loc[(units_df_rent['price'] <= rent*1.05) & (units_df_rent['price'] >= rent*0.95)]
        mean_steps_in_market = similar_units['steps_in_market'].mean()
        if np.isnan(mean_steps_in_market) or mean_steps_in_market == 0:
            # it's a thin market, so if there are no units sold in the past with a similar price profile, take the average of the entire neighbourhood
            mean_steps_in_market = units_df_rent['steps_in_market'].mean()
        return poisson.pmf(1, mean_steps_in_market)

    def probability_sale(self, price):
        """ return the probabilty of renting out the unit after 1 step in the market
        based on similar units around the same price (+/-5%)

        Args: 
            price: price to calculate the probability around 
        """
        # See also Arnott 1989
        units_df_sale = pd.DataFrame.from_records(list(np.concatenate(self.housing_units_sold).flat))
        similar_units = units_df_sale.loc[(units_df_sale['price'] <= price*1.05) & (units_df_sale['price'] >= price*0.95)]
        mean_steps_in_market = similar_units['steps_in_market'].mean()
        if np.isnan(mean_steps_in_market) or mean_steps_in_market == 0:
            # it's a thin market, so if there are no units sold in the past with a similar price profile, take the average of the entire neighbourhood
            mean_steps_in_market = units_df_sale['steps_in_market'].mean()
        return poisson.pmf(1, mean_steps_in_market)

    # def register_trade(self, housing_unit: HousingUnitAgent, new_owner: InvestorAgent, final_price: float):
    def register_trade(self, housing_unit, new_owner, final_price):
        """ Transfers the ownership of the housing unit, and registers data with the realtor
        Args: 
            housing_unit: the housing unit being traded 
            new_owner: the new InvestorAgent who is the owner of the property 
            final_price: the price that the buyer and seller settled on. To be normalized to price/m^2 then registered
        """
        # print(f'💰 selling unit! Asking price = ${int(housing_unit.asking_price_sale)}, final price = ${int(final_price)}, diff = ${int(final_price - housing_unit.asking_price_sale)}')
        # print(f'💰 selling unit! New owner = {new_owner}')
        assert new_owner.breed == 'buyer', 'Selling to a non-buyer! that\'s illegal!'

        record = {
            'unit_id': housing_unit.unique_id,
            'transaction type': 'sale',
            'area': housing_unit.area,
            'price': final_price,
            'steps_in_market': housing_unit.steps_in_market,
            'step': self.model.schedule.steps,
        }
        new_owner.breed = 'owner'
        new_owner.withdraw_bids = True
        new_owner.housed_at = housing_unit
        housing_unit.owner.housing_units_owned.remove(housing_unit)
        housing_unit.owner = new_owner
        housing_unit.tenant = new_owner
        housing_unit.price = final_price
        housing_unit.steps_in_market = 0
        housing_unit.steps_occupied = 0
        housing_unit.for_sale = False
        housing_unit.for_rent = False
        self.units_sold.append(record)
        self.model.transaction_count_sale += 1
        self.model.transaction_count_sale_cumulative += 1
        self.model.datacollector.add_table_row('transactions', record)

    def register_rent(self, housing_unit, new_tenant, rent):
        """ Adds a tenant to the housing unit, and creates a record of the contract 

        Args: 
            housing_unit: the housing unit being traded 
            new_tenant: a HouseholdAgent 
            rent: the rental price that the renter and investor settled on. To be normalized to rent/m^2 then registered
        """
        # print(f'💸 renting unit! Asking rent = ${int(housing_unit.asking_price_rent)}, final rent = ${int(rent)}, diff = ${int(rent - housing_unit.asking_price_rent)}')
        # print(f'💸 renting unit! New tenant = {new_tenant}')
        assert new_tenant.breed == 'tenant', 'Renting to a non-tenant! that\'s illegal!'

        record = {
            'unit_id': housing_unit.unique_id,
            'transaction type': 'rent',
            'area': housing_unit.area,
            'price': rent,
            'steps_in_market': housing_unit.steps_in_market,
            'step': self.model.schedule.steps,
        }
        new_tenant.breed = 'tenant'
        new_tenant.withdraw_bids = True
        housing_unit.tenant = new_tenant #TODO potentially not true if the investor is an expat who will use the unit for 1 month a year
        housing_unit.price_rental = rent
        housing_unit.steps_in_market = 0
        housing_unit.steps_occupied = 0
        housing_unit.for_sale = False
        housing_unit.for_rent = False
        self.units_rented.append(record)
        self.model.transaction_count_rent += 1
        self.model.transaction_count_rent_cumulative += 1
        self.model.datacollector.add_table_row('transactions', record)

    def get_vacancy_rate(self):
        vacancies = [unit for unit in self.all_housing_units if unit.is_vacant]
        vacancy_rate = len(vacancies)/len(self.all_housing_units)
        return vacancy_rate

    def record_state(self):
        record = {
            'step': self.model.schedule.steps,
            'id': self.unique_id,
            'neighbourhood': self.neighbourhood,
            'geometry': self.geometry,
            'average unit area': self.average_unit_area,
            'average price': self.average_house_price_sale[-1],
            'average rent': self.average_house_price_rent[-1],
            'average price history': self.average_house_price_sale,
            'average rent history': self.average_house_price_rent,
            'vacancy rate': self.get_vacancy_rate(),
            'vacancy rate normalized': self.get_vacancy_rate()/len(self.all_housing_units),
            'unit count': len(self.all_housing_units),
        }
        self.model.datacollector.add_table_row('neighbourhoods', record)

    def step_save_records(self):
        self.housing_units_sold.append(self.units_sold.copy())
        self.housing_units_rented.append(self.units_rented.copy())
        
        self.units_sold.clear()
        self.units_rented.clear()

    def step(self):
        # update average prices 
        self.set_average_house_price_rent()
        self.set_average_house_price_sale()

        # update trends dict 
        self.set_price_trend_rent()
        self.set_price_trend_sale()
        