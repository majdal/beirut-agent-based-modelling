import matplotlib.pyplot as plt
import numpy as np 

from MoneyModel import MoneyModel
from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer

# all_wealth = []

# for j in range(100): 
#     # run the model 
#     m = MoneyModel(10)
#     for i in range(10):
#         m.step()

#     # agent_wealth = [a.wealth for a in m.schedule.agents]
#     for agent in m.schedule.agents: 
#         all_wealth.append(agent.wealth)

# plt.hist(all_wealth, bins=range(max(all_wealth)+1))

# plt.show()

model = MoneyModel(10, 15, 15)
for i in range(20):
    model.step()

agent_counts = np.zeros((model.grid.width, model.grid.height))

for cell in model.grid.coord_iter():
    cell_content, x, y = cell
    agent_count = len(cell_content)
    agent_counts[x][y] = agent_count

# plt.imshow(agent_counts, interpolation='nearest')
# plt.colorbar()
# print(agent_counts)
# plt.show()
gini = model.datacollector.get_model_vars_dataframe()
gini.plot()

agent_wealth = model.datacollector.get_agent_vars_dataframe()

