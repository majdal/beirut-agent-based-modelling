from scipy.spatial import distance

from mesa.datacollection import DataCollector
from mesa import Agent


class LandAgent(Agent):
    """ A land parcel. Has a fixed position, doesn't move. 

    Properties: 
        environmental_amenities: green environemtnal amenities, set to 1 by default (from Table 1)
        price: the current price of the land, set to 200 by default (from Table 1)
        distance_from_cbd: grid distance from Central Business District 
    """

    def __init__(self, unique_id, model, pos, environmental_amenities=1, price=200):
        super().__init__(unique_id, model)
        self.environmental_amenities = environmental_amenities
        self.price = price
        self.asking_price = price  # = 200 by default, see Table 1
        self.bids = []
        # HACK :( using this because we're initializing the units before placing them
        self._initial_pos = pos
        self.distance_from_cbd = self.set_distance_from_cbd()
        self.proximity = self.set_proximity()
        self.proximity_normalized = self.normalize_proximity()
        self.ownership_transfer_count = 0
        self.bid_last_step = False
        self.latest_utility = 0

    def __str__(self):
        # price: {round(self.price, 1)}, at {round(self.distance_from_cbd, 2)} units away from CBD
        return f'LandAgent: {self.unique_id}'

    def step(self):
        """ Match the buyers and the sellers and register the trade 
        The algorithm is described in §3.19 of the paper 

        1. The seller gathers all bids for his property offered during the current time step 
        2. seller selects the highest of these bids.
        3. Nevertheless, the market transaction will only take place if the terms of trade are favorable to both the buyer and the seller
        4. new price = arithmetic average of the seller's price and the highest offer-bid of a buyer.


        +++ A successful trade is registered
        1. both buyer and seller update their status 
           the seller will not sell in the next period, 
           the buyer will not search for a land lot to buy, 
           and they will not be counted in the estimation of NB and NS in epsilon 
        3. the ownership rights on the spatial good are transferred from seller to buyer, 
        4. the transaction price is registered as the actual price for this specific land lot. 
        """
        # make a match
        # If any bids have been made
        if self.bids:
            self.bid_last_step = True
            # sort the bids lowest to heighest
            self.bids.sort(key=lambda tup: tup[1])
            top_bid = self.bids.pop()
            top_bid_agent, top_bid_price = top_bid
            # if the bid price is heigher or equal to the asking price, register the trade
            if top_bid_price >= self.asking_price:
                self.ownership_transfer_count += 1
                # The the seller selects the highest bid.
                # if the bid is successful:
                # 1. Switch the owner
                # self.model.grid.remove_agent(self.owner)
                # self.model.grid.place_agent(top_bid_agent, self.pos)
                # HACK used this method instead of .remove_agent() and .place_agent() because it was occasionally skipping
                self.model.grid[self.pos[0]][self.pos[1]].remove(self.owner)
                self.model.grid[self.pos[0]][self.pos[1]].add(top_bid_agent)

                # exclude the owner and the seller from market transactions at the next time step
                self.owner.is_selling = False
                self.owner.is_buying = False
                top_bid_agent.is_buying = False
                top_bid_agent.is_selling = False

                # 2. Increment hhe trade count
                self.model.successful_trades += 1

                # 3. New sale price of the land is the average of the asking price and the highest bid
                self.price = (top_bid_price + self.asking_price) / 2
            # clear the bids at the end of each time step
            self.bids.clear()
        else:
            self.bid_last_step = False

    @property
    def transport_cost(self):
        return self.distance_from_cbd * self.model.tcu

    def set_proximity(self):
        """ Set proximity, as described in paragraph 3.7 equation 1
        """
        return self.model.max_distance + 1 - self.distance_from_cbd

    def normalize_proximity(self):
        """ Normalized proximity, as described in paragraph 3.7 equation 2
        """
        return self.proximity / self.model.max_proximity

    @property
    def owner(self):
        cell_contents = self.model.grid.get_cell_list_contents(self.pos)
        for agent in cell_contents:
            if type(agent) is LandUserAgent:
                return agent

    def set_distance_from_cbd(self):
        return distance.euclidean(self._initial_pos, self.model.center)


class LandUserAgent(Agent):
    """ An agent that owns, searches for, and buys land. 

    Assume that each buyer can only own one piece of land (LandAgent). 

    Properties: 
        budget_housing: The housing budget that the buyer has.
                        Assumed to be seperable from their total budget budget.
                        Set to 800 by default (See Table 1)
        pref_distance: How much weight does the buyer place on the distance preference
                       Set to 0.85 by default (See Table 1)
        pref_amenities: How much weight does the buyer place on the amenities preference
                        Set to 0.15 
                        FIXME §3.10 says that pref_distance + pref_amenities = 1, and pref_amenities > 0.5
                        but Table 1 says that pref_distance = 0.85, which means that pref_distance has to be <= 0.15
    """

    def __init__(self, unique_id, model, budget_housing=800, is_selling=True, is_buying=True):
        super().__init__(unique_id, model)
        self.budget_housing = budget_housing
        # TODO This is a uniform distribution. Experiment with normal/exponential/power law distributions
        self.pref_distance = 0.85  # See Table 2 (exp 1)
        # Pref distance + pref amenities = 1 (see §3.10)
        # self.pref_amenities = 0.15
        self.pref_amenities = self.random.uniform(0.05, 0.15)
        self.is_selling = is_selling
        self.is_buying = is_buying

    def __str__(self):
        return f'UserAgent: {self.unique_id}'

    def step_place_offers(self):
        if self.is_owner:
            if self.is_selling:
                self.current_land.asking_price = self.form_ask_price()
                self.model.market.append(self.current_land)
            else:
                # Set is_selling to true so they can enter the market at the next step
                self.is_selling = True

    def step_place_bids(self):
        if self.is_buying:
            # Filter available units for those within the budget
            # FIXME how many units does an agent sample?
            units_to_sample = self.random.randint(0, len(self.model.market))
            sampled_units = self.random.sample(self.model.market, units_to_sample if units_to_sample < 50 else 50)
            units_within_budget = [offer for offer in sampled_units if offer.asking_price <= self.budget_housing]
            # get the utility of each unit on offer
            utility_of_units = [(offer, self.get_utility(offer))
                                for offer in units_within_budget]
            # sort the units to get the unit with most utility
            utility_of_units.sort(key=lambda tup: tup[1])
            # get the last element of the list, which has the highest utility
            try:
                unit_to_bid, utility = utility_of_units.pop()
                bid_price = self.form_bid_price(
                    land_unit=unit_to_bid, utility=utility)
                unit_to_bid.bids.append((self, bid_price))
            except IndexError:
                pass
        else:
            # Set is_selling to true so they can enter the market at the next step
            self.is_buying = True

    @property
    def is_owner(self):
        """ If an agent has a position already, it is an owner
        """
        return bool(self.pos)

    @property
    def current_land(self):
        current_cell = self.model.grid.get_cell_list_contents(self.pos)
        for agent in current_cell:
            if type(agent) is LandAgent:
                return agent

    def get_transport_cost(self, distance_from_cbd):
        """ Calculate the transportation cost of the unit 
        """
        # TODO think more thoroughly about how much transportation actually costs
        return distance_from_cbd * self.model.tcu

    def get_budget_constraint(self, distance_from_cbd):
        """ Y: Net budget constraint of the buyer. 
        See paragraph 3.9 of the paper
        """
        return self.budget_housing - self.get_transport_cost(distance_from_cbd)

    def get_utility(self, land_agent):
        """ U: Cobb-Douglass function of utility of each land parcel to this particular agent, given her preferences 
        See paragraph 3.10 of the paper
        """
        environmental_amenities = land_agent.environmental_amenities
        proximity_normalized = land_agent.proximity_normalized
        utility = (environmental_amenities ** self.pref_amenities) * (proximity_normalized ** self.pref_distance)
        land_agent.latest_utility = utility
        return utility

    def wtp(self, land_unit, utility):
        """ Willingness to pay function 
        See paragraph 3.12 of the paper
        """
        top = self.get_budget_constraint(land_unit.distance_from_cbd) * utility**2
        bottom = self.model.affordability**2 + utility**2
        return top/bottom

    def _form_bid_price(self, land_unit, utility):
        """ See paragraph 3.13 of the paper
        """
        wtp = self.wtp(land_unit, utility)
        return wtp * (1 + self.model.epsilon)

    def form_bid_price(self, land_unit, utility):
        """ Combines all the steps to formulate bid price
        """
        # budget constraint factoring in transportation costs (§3.9)
        budget_constraint = self.budget_housing - land_unit.distance_from_cbd*self.model.tcu
        # willingness to pay given utility (§3.12)
        top = budget_constraint * (utility**2)
        bottom = self.model.affordability**2 + utility**2
        wtp = top/bottom
        return wtp * (1 + self.model.epsilon)

    def wta(self):
        """ Willingness to ask function 
        See paragraph 3.17 of the paper
        """
        return self.current_land.price * 1.25  # See Table 1

    def form_ask_price(self):
        """ See paragraph 3.18 of the paper
        """
        ask = self.wta() * (1 + self.model.epsilon)
        return ask if ask > self.current_land.price else self.current_land.price
