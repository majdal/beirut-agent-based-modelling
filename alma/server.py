from mesa.visualization.modules import CanvasGrid, TextElement
from mesa.visualization.ModularVisualization import ModularServer

from model import ALMAModel
from agents import LandAgent, LandUserAgent

grid_rows = 29
grid_cols = 29

n_buyers = 29*29
n_sellers = 29*29


class EpsilonElement(TextElement):
    """ Live updated epsilon value 
    """

    def render(self, model):
        return f'Current epsilon value: {round(model.epsilon, 2)}'


class BuyersSellersElement(TextElement):
    """ Live updated buyers count
    """

    def render(self, model):
        return f'{model.n_buyers} buyers, {model.n_sellers} sellers'


class SuccessfulTradesElement(TextElement):
    """ Live updated count of successful trades
    """

    def render(self, model):
        return f'{model.successful_trades} trades happened'


def env_amenities_portrayal(agent):
    """ Returns a green scale colour to be displayed by the CanvasGrid. 
    """
    assert agent is not None
    colors = ['black', 'green', 'red', 'pink']

    if type(agent) is LandAgent:
        portrayal = {"Shape": "rect", "w": 1, "h": 1, "Filled": "true", "Layer": 1}
        portrayal["x"] = agent.pos[0]
        portrayal["y"] = agent.pos[1]
        portrayal["Color"] = f'hsla(0, {0 if not agent.bid_last_step else 50}%, 20%, {agent.price/agent.model.most_expensive_land})'
        portrayal["Env amenities"] = round(agent.environmental_amenities, 3)
        portrayal["Distance"] = round(agent.distance_from_cbd, 1)
        portrayal["Transport cost"] = round(agent.transport_cost, 1)
        portrayal["asking price"] = round(agent.asking_price, 0)
        portrayal["Price"] = round(agent.price, 0)
        # portrayal["text"] = len(agent.model.grid.get_cell_list_contents(agent.pos)) - 1 #int(agent.price) #
        # portrayal["text"] = agent.ownership_transfer_count
        # portrayal["text"] = round(agent.latest_utility, 3)
        portrayal["text"] = int(agent.price)
        #portrayal["text_color"] = 'white' if agent.owner and agent.owner.is_selling else 'black'
        portrayal["text_color"] = 'white' if len(agent.model.grid.get_cell_list_contents(agent.pos))-1 == 1 else 'red'
        # portrayal["text_color"] = colors[len(agent.model.grid.get_cell_list_contents(agent.pos))-1] #'red' if agent.owner else 'black'
        portrayal["unique id "] = agent.unique_id
        portrayal["pos"] = agent.pos
        portrayal["content"] = len(agent.model.grid.get_cell_list_contents(agent.pos))
        portrayal["residents"] = [str(a) for a in agent.model.grid.get_cell_list_contents(agent.pos)]
        portrayal["utility"] = agent.latest_utility
        portrayal["transfer count"] = agent.ownership_transfer_count

        return portrayal

    elif type(agent) is LandUserAgent and agent.is_owner:
        portrayal = {"Shape": "rect", "w": 1, "h": 1, "Filled": "true", "Layer": 0}
        portrayal["x"] = agent.pos[0]
        portrayal["y"] = agent.pos[1]
        # portrayal["border_color"] = 'black' if agent.is_selling else 'white'

        portrayal["Color"] = 'black' if agent.is_selling else 'white'
        # portrayal["text"] = 's' if agent.is_selling else '' #agent.current_land.price  # round(agent.asking_price, 0) if agent.asking_price else ''
        # portrayal["text_color"] = 'red'

        # return portrayal


env_amenities_element = CanvasGrid(env_amenities_portrayal, grid_rows, grid_cols)
epsilon_element = EpsilonElement()
buyers_sellers_element = BuyersSellersElement()
successful_trades_element = SuccessfulTradesElement()


server = ModularServer(
    ALMAModel,
    [epsilon_element,  buyers_sellers_element,
        env_amenities_element, successful_trades_element],
    "ALMA model",
    {"width": grid_rows, "height": grid_cols, "tcu": 5,
        "buyers": n_buyers, "sellers": n_sellers},
)
