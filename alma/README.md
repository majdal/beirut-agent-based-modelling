# ALMA model 

This is a Mesa implementation of the ALMA (Agent-based Land MArket) model described by Filatova et al in [this](http://jasss.soc.surrey.ac.uk/12/1/3.html) paper from 2009. It describes a land market that resembles the Alfonso/Von Thünen model of the concentric city. 

Maybe if I get the time, I will implement the ODD + D protocol for this model as well. 


To run this model, make sure that you have Mesa installed, then run `mesa runserver`. 