import math
from datetime import datetime

from mesa import Model
from mesa.space import MultiGrid
from mesa.time import StagedActivation, RandomActivation
from mesa.datacollection import DataCollector

from agents import LandAgent, LandUserAgent


def capture_prices(model):
    price_grid = []
    for row in model.grid.grid:
        new_row = []
        for cell in row:
            for item in cell:
                try:
                    new_row.append(item.price)
                except:
                    pass
        price_grid.append(new_row)
    return price_grid

class ALMAModel(Model):
    """ Agent-based Land MArket model. 

    This is a Mesa implementation of the ALMA (Agent-based Land MArket) model described by 
    Filatova and Parker in their 2009 paper published in JASSS. 

    It describes a land market that resembles the Alfonso/Von Thünen model of the concentric city. 

    http://jasss.soc.surrey.ac.uk/12/1/3.html

    """

    def __init__(self, width=25, height=25, tcu=1, buyers=841, sellers=841, affordability=0.90):
        """ Create a new model. 
        Number of owners is equal to the number of cells. Buyers are ex

        Args: 
            width, height: dimensions of the grid
            tcu: transportation cost per unit distance 
            affordability: an proxy for the affordability of the market, see Table 1
        """
        super().__init__()
        # width and height = 29 as in Table 1
        self.width = width
        self.height = height
        self.n_cells = self.width * self.height
        self.successful_trades = 0
        self.successful_trades_history = [0, 1, 2, 3, 4, 5, 6, 7]
        self.affordability = affordability  # 70 - See Table 1
        self.grid = MultiGrid(width, height, False)
        self.bidding_schedule = StagedActivation(self,
                                                 ['step_place_offers', 'step_place_bids', ],
                                                 shuffle=True)
        self.matching_schedule = RandomActivation(self)

        self.center = (math.floor(width/2), math.floor(height/2))
        self.tcu = tcu
        self.n_buyers = buyers
        self.n_sellers = sellers

        # Maximum distance is half the length of the diagonal
        self.max_distance = ((width**2 + height**2)**0.5) / 2
        self.max_proximity = self.max_distance + 1 - 0  # see equation 2

        self.market = []
        self.most_expensive_land = 0
        self.epsilon = None

        unique_id = 1

        for cell in self.grid.coord_iter():
            pos = (cell[1], cell[2])

            # Start by assigning land lots
            parcel = LandAgent(unique_id, self, pos)
            self.grid.place_agent(parcel, pos)
            self.matching_schedule.add(parcel)

            # Assign owners to each patch
            owner = LandUserAgent(unique_id, self)
            self.grid.place_agent(owner, pos)
            self.bidding_schedule.add(owner)

            # increment the ids
            unique_id += 1

        self.set_epsilon()
        self.set_most_expensive_land()

        self.data_collector = DataCollector(model_reporters={"prices": capture_prices})

    @property
    def sellers(self):
        # return filter(lambda agent: agent.is_selling, self.bidding_schedule.agents)
        return [land_user for land_user in self.bidding_schedule.agents if land_user.is_selling]

    @property
    def buyers(self):
        # return filter(lambda agent: agent.is_selling, self.bidding_schedule.agents)
        return [land_user for land_user in self.bidding_schedule.agents if land_user.is_buying]

    @property
    def floaters(self):
        # return filter(lambda agent: not agent.is_owner, self.bidding_schedule.agents)
        return [agent for agent in self.bidding_schedule.agents if not agent.is_owner]

    def set_epsilon(self):
        """ A proxy for the strength of demand 
        """
        self.n_sellers = len(self.sellers)
        self.n_buyers = len(self.buyers)
        self.epsilon = (self.n_buyers - self.n_sellers) / \
                       (self.n_buyers + self.n_sellers)

    def set_most_expensive_land(self):
        for land in self.matching_schedule.agents:
            if land.price > self.most_expensive_land:
                self.most_expensive_land = land.price

    def step(self):
        """ Advance the model by a single step. 
        """
        self.set_epsilon()
        self.set_most_expensive_land()
        # At the step, each agent does the following
        # 1. if they're selling, form an asking price,
        #    if they're buying they place their bids
        self.bidding_schedule.step()
        # 2. The highest bidder gets the land
        # print(f'parcels on sale: {len(self.market)}')
        self.matching_schedule.step()
        # 3. A new sample of sellers is selected

        # for land_user in self.bidding_schedule.agents:
        #     land_user.is_buying = True
        #     land_user.is_selling = True
        # n_new_sellers = self.random.randrange(1, int(self.n_cells*0.05))
        # new_sellers = self.random.sample(self.matching_schedule.agents, n_new_sellers)
        # for seller in new_sellers:
        #     seller.owner.is_selling = True
        # n_sellers = [land_user for land_user in self.bidding_schedule.agents if land_user.is_selling]

        # # 4. A new sample of buyers is selected
        # floaters = self.floaters
        # for buyer in floaters:
        #     buyer.is_buying = True
        # 5. Clear the market of all the bids
        self.market.clear()

        self.successful_trades_history.pop(0)
        self.successful_trades_history.append(self.successful_trades)
        
        # capture all data
        self.data_collector.collect(self)
        # if 8 steps pass without trades, stop the simulation 
        if len(set(self.successful_trades_history)) == 1:
            self.running = False
            alma_data = self.data_collector.get_model_vars_dataframe()
            alma_data.to_csv(f'./alma_data_{datetime.now().isoformat()}.csv')
