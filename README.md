# Beirut Agent Based Modelling

This repository contains the model for my thesis to complete my master's in Urban Planning and Policy at the American University of Beirut. 


## Getting started

To get started, you need to run the following commands to start a virtual evironment and activate it. 

0. Copy this repository to your preferred location. Also, make sure that you have python3 installed. Specific instructions on how to do that are [here](https://installpython3.com/). 
1. In your repository, run the following command `mkdir .venv && python3 -m venv .venv`. This creates a directory called `.venv`, places a virtual environment inside of it. 
2. Activate the virtual environment (you'll need to do this every time you want to work on the project). Use the following command: `source .venv/bin/activate`
3. Install the requirements using `pip install -r requirements.txt` (you'll do this only once!)
4. Go to one of the folders, each of which is a different model. There, run the mesa command to start the server, `mesa runserver`
5. That's it! You're all set. 

