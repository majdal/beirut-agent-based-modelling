from mesa_geo.visualization.MapModule import MapModule
from mesa_geo.visualization.ModularVisualization import ModularServer

from model import BeirutBuildingsModel

def draw(agent):
    return {'color': 'Blue'}

map_element = MapModule(draw, [33.8861,35.5040], 15, 500, 500)

server = ModularServer(
    BeirutBuildingsModel, [map_element,], "Beirut",
)

server.launch()

