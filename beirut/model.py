import requests 

from mesa import Model 
from mesa_geo import GeoSpace, AgentCreator

from agents import Building

class BeirutBuildingsModel(Model):
    def __init__(self):
        self.grid = GeoSpace()

        # data = 'https://overpass-api.de/api/interpreter?data=%2F*%0AThis%20has%20been%20generated%20by%20the%20overpass-turbo%20wizard.%0AThe%20original%20search%20was%3A%0A%E2%80%9Cbuilding%3D*%E2%80%9D%0A*%2F%0A%5Bout%3Ajson%5D%5Btimeout%3A25%5D%3B%0A%2F%2F%20gather%20results%0A%28%0A%20%20%2F%2F%20query%20part%20for%3A%20%E2%80%9Cbuilding%3D*%E2%80%9D%0A%20%20node%5B%22building%22%5D%2833.866210798540344%2C35.44567108154297%2C33.921426204389%2C35.539913177490234%29%3B%0A%20%20way%5B%22building%22%5D%2833.866210798540344%2C35.44567108154297%2C33.921426204389%2C35.539913177490234%29%3B%0A%20%20relation%5B%22building%22%5D%2833.866210798540344%2C35.44567108154297%2C33.921426204389%2C35.539913177490234%29%3B%0A%29%3B%0A%2F%2F%20print%20results%0Aout%20body%3B%0A%3E%3B%0Aout%20skel%20qt%3B'
        # r = requests.get(data)
        # geojson_buildings = r.json()
        AC = AgentCreator(agent_class=Building, agent_kwargs={'model': self}) #, crs="epsg:3857"
        agents = AC.from_file('./data/buildings_sample.geojson', unique_id='@id')
        #AC.from_GeoJSON(GeoJSON=geojson_buildings, unique_id='@id')
        self.grid.add_agents(agents)